��    W      �     �      �     �     �     �  �   �     '     4     G     N  J   b     �     �     �     �     �     �     �     �     		     	     ;	  W   D	     �	  	   �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     �	     
     '
     C
     Q
     ]
  $   w
     �
     �
     �
     �
     �
  	   �
     �
     �
                     $     +     ;     P     ^  	   c     m     {     �  )   �  
   �     �  L   �  	     
   )     4     E     J     [  	   i  	   s  *   }     �     �     �     �  
   �     �     �     �            �     	   �     �     �  }       �     �     �  �   �     %     2     E     L  L   `     �     �     �     �     �     �     �     �     	          ;  W   D     �  	   �     �     �     �     �     �     �     �     �     �     �          '     A     O     [  $   u     �     �     �     �     �  	   �     �     �     �               "     )     9     N     \  	   a     k     y     �  )   �  
   �     �  M   �  	     
   (     3     D     I     Z  	   h  	   r  *   |     �     �     �     �  
   �     �     �     �            �     	   �     �     �     $          .                       O          V       ;   %   )   N               5      4   -   /       @          B   #       !           "   	                  
   *             M   ?           1   6       3      D          =          '       I   R               Q       2   &   C                           H                    P      7          A   S   L       0   :   U          F              T   E   +   9   8   (   G              W   <   ,   >             K   J        Action Actions Add New Add subscription forms on website, send HTML newsletters & automatically notify subscribers about new blog posts once it is published. All Statuses Available Keywords Cancel Check CSV structure Choose a FROM email address for all the emails to be sent from this plugin Delete Description Display Name Field Double Opt-In Edit Edit Templates Email Email Address Email Subscribers Email Subscribers & Newsletters End Date Enter the admin email addresses that should receive notifications (separated by comma). Export Full Size Group Help & Info Icegram Import Medium Size NO Name New Templates Next No Custom Post Types Available No Templates found No Templates found in Trash Notifications Placeholder Please select categories. Please select subscribers to update. Post Notification Post Notifications Preview Preview Email Preview Template Rainmaker Reports Roles Save Save Settings Search Templates Select Select CSV file Select Post Category Select Status Sent Sent Date Set thumbnail Settings Short description Short description about subscription form Start Date Status Subject for the admin email whenever a new contact signs up and is confirmed Subscribe Subscribed Subscriber Group Sync Template Preview Template Type Templates Thumbnail Thumbnail (For Visual Representation only) Total Emails Sent Type Unconfirmed Unsubscribed User Roles View Templates Viewed Date Viewed Status Widget Title YES Your subscription was successful! Kindly check your mailbox and confirm your subscription. If you don't see the email within a few minutes, check the spam/junk folder. from here https://www.icegram.com/ timezone date formatY-m-d PO-Revision-Date: 2020-09-25 12:08:24+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: GlotPress/3.0.0-alpha.2
Language: en_GB
Project-Id-Version: Plugins - Email Subscribers &amp; Newsletters &#8211; Simple and Effective Email Marketing WordPress Plugin - Stable (latest release)
 Action Actions Add New Add subscription forms on website, send HTML newsletters, and automatically notify subscribers about new blog posts once they are published. All Statuses Available Keywords Cancel Check CSV structure Choose a FROM e-mail address for all the e-mails to be sent from this plugin Delete Description Display Name Field Double Opt In Edit Edit Templates Email Email Address Email Subscribers Email Subscribers & Newsletters End Date Enter the admin email addresses that should receive notifications (separated by comma). Export Full Size Group Help & Info Icegram Import Medium Size NO Name New Templates Next No Custom Post Types Available No Templates found No Templates found in Bin Notifications Placeholder Please select categories. Please select subscribers to update. Post Notification Post Notifications Preview Preview Email Preview Template Rainmaker Reports Roles Save Save Settings Search Templates Select Select CSV file Select Post Category Select Status Sent Sent Date Set thumbnail Settings Short description Short description about subscription form Start Date Status Subject for the admin e-mail whenever a new contact signs up and is confirmed Subscribe Subscribed Subscriber Group Sync Template Preview Template Type Templates Thumbnail Thumbnail (For Visual Representation only) Total E-mails Sent Type Unconfirmed Unsubscribed User Roles View Templates Viewed Date Viewed Status Widget Title YES Your subscription was successful! Kindly check your mailbox and confirm your subscription. If you don't see the e-mail within a few minutes, check the spam/junk folder. from here https://www.icegram.com/ Y-m-d 