<?php
$property_size      =   wpestate_get_converted_measure( $post->ID, 'property_size' );
$property_rooms     =   get_post_meta($post->ID,'property_rooms',true);
$property_bedrooms  =   get_post_meta($post->ID,'property_bedrooms',true);
$prop_id            =   $post->ID;  
?>
    
<div class="property_listing_details">
    <?php 
        if($property_rooms!=''){
            print ' <div class="inforoom_unit_type3">'.esc_html($property_rooms).' '.esc_html__('rooms','wpresidence').'</div>';
        }

        if($property_bedrooms!=''){
            print '<div class="infobath_unit_type3">'.esc_html($property_bedrooms).' '.esc_html__('beds','wpresidence').'</div>';
        }

        if($property_size!=''){
            print ' <div class="infosize_unit_type3">'.trim($property_size).'</div>';//escaped above
        }

    ?>
</div> 