<?php
global $prop_id ;
global $agent_email;
global $agent_urlc;
global $link;
global $agent_url;
global $agent_urlc;
global $link;
global $agent_facebook;
global $agent_posit;
global $agent_twitter; 
global $agent_linkedin; 
global $agent_instagram;
global $agent_pinterest; 
global $agent_member;



$post_author_id = get_post_field( 'post_author', $post_id );

if ( get_the_author_meta('user_level') !=10){

    $preview_img    =   get_the_author_meta( 'custom_picture' ,$post_author_id );
    if($preview_img==''){
        $preview_img=get_theme_file_uri('/img/default-user.png');
    }

    $agent_skype         = get_the_author_meta( 'skype',$post_author_id  );
    $agent_phone         = get_the_author_meta( 'phone' ,$post_author_id  );
    $agent_mobile        = get_the_author_meta( 'mobile' ,$post_author_id  );
    $agent_email         = get_the_author_meta( 'user_email',$post_author_id   );
    $agent_pitch         = '';
    $agent_posit         = get_the_author_meta( 'title' ,$post_author_id  );
    $agent_facebook      = get_the_author_meta( 'facebook' ,$post_author_id  );
    $agent_twitter       = get_the_author_meta( 'twitter' ,$post_author_id  );
    $agent_linkedin      = get_the_author_meta( 'linkedin' ,$post_author_id  );
    $agent_pinterest     = get_the_author_meta( 'pinterest',$post_author_id   );
    $agent_instagram     = get_the_author_meta( 'instagram' ,$post_author_id  );
    $agent_urlc          = get_the_author_meta( 'website' ,$post_author_id  );
    $link                =  esc_url( get_permalink() );
    $name                = get_the_author_meta( 'first_name',$post_author_id  ).' '.get_the_author_meta( 'last_name',$post_author_id );

    include( locate_template('templates/agentdetails.php'));
    include( locate_template('templates/agent_contact.php') );        

}

?>