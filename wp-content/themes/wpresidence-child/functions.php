<?php 

add_action( 'init', 'c7_custom_init' );
function c7_custom_init() {
    //ini_set('display_errors', 1);
    //ini_set('display_startup_errors', 1);
    //error_reporting(E_ALL);

    register_post_type(
        'contacto', array(
            'labels'        =>  array('name' => 'Contactos', 'singular_name' => 'Contacto'),
            'public'        =>  TRUE,
            'rewrite'       =>  array( 'slug' => 'contacto' ),
            'menu_icon'     =>  'dashicons-admin-users',
            'has_archive'   =>  TRUE,
            'supports'      =>  array( 'title', 'editor')
        )
    );

}

function c7_child_scripts(){
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('bootstrap.min', 'bootstrap-theme.min') );
}
add_action( 'wp_enqueue_scripts', 'c7_child_scripts');

add_action('template_redirect', 'c7_template_redirect');
function c7_template_redirect() {
    if (is_page('publicar-propiedad') && !is_user_logged_in()) {
        wp_redirect(home_url());die;
    } else if (!empty($_GET['redirect']) && $_GET['redirect']=='redes' && $_GET['social']) {
        contar_visitas_redes($_GET['social']);
        if ($_GET['social'] == 'facebook') {
            wp_redirect('https://www.madretierra.pe/');die;
        } else if ($_GET['social'] == 'youtube') {
            wp_redirect('https://www.madretierra.pe/');die;
        } else if ($_GET['social'] == 'instagram') {
            wp_redirect('https://www.madretierra.pe/');die;
        } else if ($_GET['social'] == 'whatsapp') {
            wp_redirect('https://api.whatsapp.com/send?phone=987654321&text=Hola,%20necesito%20info%20de%20'.home_url());die;
        }
    } else if (is_singular('estate_property')) {
        $idpost = get_the_id();
        contar_visitas_propiedades($idpost);
    } else if (is_page_template('front_property_submit.php') && $_POST) {
        $fecha = date('Y-m-d');
        $mes = date('Y-m');

        $visitas_usuarios = get_option('visitas_usuarios');
        if (empty($visitas_usuarios)) $visitas_usuarios = array();
        if (empty($visitas_usuarios[$fecha])) $visitas_usuarios[$fecha] = 0;
        $visitas_usuarios[$fecha]++;
        update_option('visitas_usuarios', $visitas_usuarios);

        $visitas_usuarios_mes = get_option('visitas_usuarios_mes');
        if (empty($visitas_usuarios_mes)) $visitas_usuarios_mes = array();
        if (empty($visitas_usuarios_mes[$mes])) $visitas_usuarios_mes[$mes] = 0;
        $visitas_usuarios_mes[$mes]++;
        update_option('visitas_usuarios_mes', $visitas_usuarios_mes);
    }
    /*if (is_cart() && pll_current_language() == 'es') {
        foreach( WC()->cart->get_cart() as $cart_item ){
            $product_id = $cart_item['product_id'];
            if (pll_get_post_language( $product_id)) {
                wp_redirect(home_url('en/cart'));die;
            }
        }
    } else if (is_checkout() && pll_current_language() == 'es') {
        foreach( WC()->cart->get_cart() as $cart_item ){
            $product_id = $cart_item['product_id'];
            if (pll_get_post_language( $product_id)) {
                wp_redirect(home_url('en/checkout'));die;
            }
        }
    }*/
}

function contar_visitas_propiedades($idpost) {
    $fecha = date('Y-m-d');
    $mes = date('Y-m');

    // Sumar visitas diarias
    $visitas = get_post_meta($idpost, 'visitas', true);
    if (empty($visitas)) $visitas = array();
    if (empty($visitas[$fecha])) $visitas[$fecha] = 0;
    $visitas[$fecha]++;
    update_post_meta($idpost, 'visitas', $visitas);

    $visitas_mes = get_post_meta($idpost, 'visitas_mes', true);
    if (empty($visitas_mes)) $visitas_mes = array();
    if (empty($visitas_mes[$mes])) $visitas_mes[$mes] = 0;
    $visitas_mes[$mes]++;
    update_post_meta($idpost, 'visitas_mes', $visitas_mes);

    // Sumar visitas por categoria
    $category = wp_get_post_terms($idpost, 'property_category');

    $visitas_category = get_term_meta($category[0]->term_id, 'visitas', true);
    if (empty($visitas_category)) $visitas_category = array();
    if (empty($visitas_category[$fecha])) $visitas_category[$fecha] = 0;
    $visitas_category[$fecha]++;
    update_term_meta($category[0]->term_id, 'visitas', $visitas_category);

    $visitas_mes_category = get_term_meta($category[0]->term_id, 'visitas_mes', true);
    if (empty($visitas_mes_category)) $visitas_mes_category = array();
    if (empty($visitas_mes_category[$mes])) $visitas_mes_category[$mes] = 0;
    $visitas_mes_category[$mes]++;
    update_term_meta($category[0]->term_id, 'visitas_mes', $visitas_mes_category);

    //echo '<pre>';print_r($visitas);echo '</pre>';die;
}

function contar_visitas_redes($social) {
    $fecha = date('Y-m-d');
    $mes = date('Y-m');
    if ($social == 'facebook') {

        $visitas_facebook = get_option('visitas_facebook');
        if (empty($visitas_facebook)) $visitas_facebook = array();
        if (empty($visitas_facebook[$fecha])) $visitas_facebook[$fecha] = 0;
        $visitas_facebook[$fecha]++;
        update_option('visitas_facebook', $visitas_facebook);

        $visitas_facebook_mes = get_option('visitas_facebook_mes');
        if (empty($visitas_facebook_mes)) $visitas_facebook_mes = array();
        if (empty($visitas_facebook_mes[$mes])) $visitas_facebook_mes[$mes] = 0;
        $visitas_facebook_mes[$mes]++;
        update_option('visitas_facebook_mes', $visitas_facebook_mes);

    } else if ($social == 'youtube') {

        $visitas_youtube = get_option('visitas_youtube');
        if (empty($visitas_youtube)) $visitas_youtube = array();
        if (empty($visitas_youtube[$fecha])) $visitas_youtube[$fecha] = 0;
        $visitas_youtube[$fecha]++;
        update_option('visitas_youtube', $visitas_youtube);

        $visitas_youtube_es = get_option('visitas_youtube_es');
        if (empty($visitas_youtube_es)) $visitas_youtube_es = array();
        if (empty($visitas_youtube_es[$mes])) $visitas_youtube_es[$mes] = 0;
        $visitas_youtube_es[$mes]++;
        update_option('visitas_youtube_es', $visitas_youtube_es);

    } else if ($social == 'instagram') {

        $visitas_instagram = get_option('visitas_instagram');
        if (empty($visitas_instagram)) $visitas_instagram = array();
        if (empty($visitas_instagram[$fecha])) $visitas_instagram[$fecha] = 0;
        $visitas_instagram[$fecha]++;
        update_option('visitas_instagram', $visitas_instagram);

        $visitas_instagram_mes = get_option('visitas_instagram_mes');
        if (empty($visitas_instagram_mes)) $visitas_instagram_mes = array();
        if (empty($visitas_instagram_mes[$mes])) $visitas_instagram_mes[$mes] = 0;
        $visitas_instagram_mes[$mes]++;
        update_option('visitas_instagram_mes', $visitas_instagram_mes);

    } else if ($social == 'whatsapp') {

        $visitas_whatsapp = get_option('visitas_whatsapp');
        if (empty($visitas_whatsapp)) $visitas_whatsapp = array();
        if (empty($visitas_whatsapp[$fecha])) $visitas_whatsapp[$fecha] = 0;
        $visitas_whatsapp[$fecha]++;
        update_option('visitas_whatsapp', $visitas_whatsapp);

        $visitas_whatsapp_mes = get_option('visitas_whatsapp_mes');
        if (empty($visitas_whatsapp_mes)) $visitas_whatsapp_mes = array();
        if (empty($visitas_whatsapp_mes[$mes])) $visitas_whatsapp_mes[$mes] = 0;
        $visitas_whatsapp_mes[$mes]++;
        update_option('visitas_whatsapp_mes', $visitas_whatsapp_mes);

    }
}

function post_published_propiedades_reportes( $idpost, $post ) {
    $fecha = date('Y-m-d');
    $mes = date('Y-m');

    $visitas_admin = get_option('visitas_admin');
    if (empty($visitas_admin)) $visitas_admin = array();
    if (empty($visitas_admin[$fecha])) $visitas_admin[$fecha] = 0;
    $visitas_admin[$fecha]++;
    update_option('visitas_admin', $visitas_admin);

    $visitas_admin_mes = get_option('visitas_admin_mes');
    if (empty($visitas_admin_mes)) $visitas_admin_mes = array();
    if (empty($visitas_admin_mes[$mes])) $visitas_admin_mes[$mes] = 0;
    $visitas_admin_mes[$mes]++;
    update_option('visitas_admin_mes', $visitas_admin_mes);

}
add_action( 'publish_estate_property', 'post_published_propiedades_reportes', 10, 2 );

add_action('wp_footer', 'c7_wp_footer');
function c7_wp_footer() {
    if(pll_current_language() == 'es') { ?>
        <script>
            jQuery(function($) {
                $(".single-estate_property #wpestate_property_description_section h4").text('Descripción')
            })
        </script>
    <?php } ?>
    <script>
        jQuery(function($) {
            if ($('.page-template-front_property_submit #prop_category_submit').length > 0) {
                $('.page-template-front_property_submit #prop_category_submit option:first-child').remove()
                $('.page-template-front_property_submit #prop_action_category_submit option:first-child').remove()
            }
            if ($('.page-template-user_dashboard_add #prop_category_submit').length > 0) {
                $('.page-template-user_dashboard_add #prop_category_submit option:first-child').remove()
                $('.page-template-user_dashboard_add #prop_action_category_submit option:first-child').remove()
                $('.page-template-user_dashboard_add #tipo-estacionamientos option:first-child').remove()
            }
        })
    </script>
<?php }

add_action('wp_ajax_nopriv_get_dpto', 'ajax_get_dpto');
add_action('wp_ajax_get_dpto', 'ajax_get_dpto');
function ajax_get_dpto(){
    if ( ! wp_verify_nonce( $_POST['wpnonce'], 'get_dpto' ) ) die ( 'Hubo un error, recargue la página');
    global $wpdb;
    $provincias = $wpdb->get_col("SELECT prov FROM ubigeo WHERE dpto = '{$_POST['dpto']}' GROUP BY prov");
    if ($provincias) {
        foreach ($provincias as $provincia) { ?>
            <option value="<?php echo $provincia; ?>"><?php echo $provincia; ?></option>
        <?php }
    }
    die;
}

add_action('wp_ajax_nopriv_get_prov', 'ajax_get_prov');
add_action('wp_ajax_get_prov', 'ajax_get_prov');
function ajax_get_prov(){
    if ( ! wp_verify_nonce( $_POST['wpnonce'], 'get_prov' ) ) die ( 'Hubo un error, recargue la página');
    global $wpdb;
    $distritos = $wpdb->get_col("SELECT distrito FROM ubigeo WHERE prov = '{$_POST['prov']}' AND  dpto = '{$_POST['dpto']}'");
    if ($distritos) {
        foreach ($distritos as $distrito) { ?>
            <option value="<?php echo $distrito; ?>"><?php echo $distrito; ?></option>
        <?php }
    }
    die;
}

add_action('wp_ajax_nopriv_guardar_contacto', 'ajax_guardar_contacto');
add_action('wp_ajax_guardar_contacto', 'ajax_guardar_contacto');
function ajax_guardar_contacto(){
    if ( ! wp_verify_nonce( $_POST['wpnonce'], 'guardar_contacto' ) ) die ( 'Hubo un error, recargue la página');
    $id = wp_insert_post(array(
        'post_title' => $_POST['nombre'],
        'post_type' => 'contacto',
        'post_content' => $_POST['mensaje'],
        'post_status' => 'publish'
    ));
    add_post_meta($id, 'email', $_POST['email']);
    add_post_meta($id, 'telefono', $_POST['telefono']);
    die;
}

add_action('wp_ajax_nopriv_enviar_reserva', 'ajax_enviar_reserva');
add_action('wp_ajax_enviar_reserva', 'ajax_enviar_reserva');
function ajax_enviar_reserva(){
    if ( ! wp_verify_nonce( $_POST['wpnonce'], 'enviar_reserva' ) ) die ( 'Hubo un error, recargue la página');
    $headers = 	array(
        'Content-Type: text/html; charset=UTF-8',
        'From: Madre Tierra <no-reply@madretierra.pe>' . "\r\n"
    );

    $message = '<p>Hola,<br>';
    $message .= 'Se hizo una nueva reserva en la web de la siguiente propiedad:</p>';
    $message .= '<p><strong>Propiedad:</strong> '.$_POST['propiedad'].'</p>';
    $message .= '<p>Los datos de la persona son los siguientes:</p>';
    $message .= '<p><strong>Nombre:</strong> '.$_POST['nombre'].'<br>';
    $message .= '<strong>Teléfono:</strong> '.$_POST['telefono'].'<br>';
    $message .= '<strong>Email:</strong> '.$_POST['email'].'</p>';

    //wp_mail('cesar7492@gmail.com', 'Reservaron la propiedad '.$_POST['propiedad'], $message, $headers);
    wp_mail(get_bloginfo('admin_email'), 'Reservaron la propiedad '.$_POST['propiedad'], $message, $headers);
    die;
}

function wpestate_ajax_register_user(){
        check_ajax_referer( 'wpestate_ajax_log_reg', 'security' );
        $type       =   intval( $_POST['type'] );
        $capthca    =   sanitize_text_field ( $_POST['capthca'] );
        
        if( wpresidence_get_option('wp_estate_use_captcha','')=='yes'){
            if(!isset($_POST['capthca']) || $_POST['capthca']==''){
                exit( esc_html__('wrong captcha','wpresidence') );
            }

            $secret    = wpresidence_get_option('wp_estate_recaptha_secretkey','');
            global $wp_filesystem;
            if (empty($wp_filesystem)) {
                require_once (ABSPATH . '/wp-admin/includes/file.php');
                WP_Filesystem();
            }
            $response = $wp_filesystem->get_contents( wpestate_recaptcha_path($secret,$captcha) );

            $response=json_decode(json_decode);
            if ($response['success'] = false) {
                exit('out pls captcha');
            }
        }
        
      
        $allowed_html               =   array();
        $user_email                 =   trim( sanitize_text_field(wp_kses( $_POST['user_email_register'] ,$allowed_html) ) );
        $user_name                  =   trim( sanitize_text_field(wp_kses( $_POST['user_login_register'] ,$allowed_html) ) );
        $enable_user_pass_status    =   esc_html ( wpresidence_get_option('wp_estate_enable_user_pass','') );
        
        $new_user_type              =   intval($_POST['new_user_type']);
      
        
        if (preg_match("/^[0-9A-Za-z_]+$/", $user_name) == 0) {
            print esc_html__('Invalid username (do not use special characters or spaces)!','wpresidence');
            die();
        }
        
        if ($user_email=='' || $user_name=='' ){
            print esc_html__('Username and/or Email field is empty!','wpresidence');
            exit();
        }
        
        if(filter_var($user_email,FILTER_VALIDATE_EMAIL) === false) {
            print esc_html__('The email doesn\'t look right !','wpresidence');
            exit();
        }
        
        $domain = mb_substr(strrchr($user_email, "@"), 1);
        if( !checkdnsrr ($domain) ){
            print esc_html__('The email\'s domain doesn\'t look right.','wpresidence');
            exit();
        }
        
        
        $user_id     =   username_exists( $user_name );
        if ($user_id){
            print esc_html__('Username already exists.  Please choose a new one.','wpresidence');
            exit();
        }
        
        if($enable_user_pass_status=='yes' ){
            $user_pass              =   trim( sanitize_text_field(wp_kses( $_POST['user_pass'] ,$allowed_html) ) );
            $user_pass_retype       =   trim( sanitize_text_field(wp_kses( $_POST['user_pass_retype'] ,$allowed_html) ) );
        
            if ($user_pass=='' || $user_pass_retype=='' ){
                print esc_html__('One of the password field is empty!','wpresidence');
                exit();
            }
            
            if ($user_pass !== $user_pass_retype ){
                print esc_html__('Passwords do not match','wpresidence');
                exit();
            }
        }
         
 
         
        if ( !$user_id and email_exists($user_email) == false ) {
            if($enable_user_pass_status=='yes' ){
                $user_password = $user_pass; // no so random now!
            }else{
                $user_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
            }
            
            $user_id         = wp_create_user( $user_name, $user_password, $user_email );
         
            if ( is_wp_error($user_id) ){
        
            }else{
                if($enable_user_pass_status=='yes' ){
                    print esc_html__('Your account was created and you can login now!','wpresidence');
                }else{
                    print esc_html__('An email with the generated password was sent!','wpresidence');
                }
                  
                wpestate_update_profile($user_id);
                wpestate_wp_new_user_notification( $user_id, $user_password ) ;
                update_user_meta( $user_id, 'user_estate_role', $new_user_type) ;
             
                if($new_user_type!==0 && $new_user_type!==1 ){  
                    wpestate_register_as_user($user_name,$user_id,$new_user_type);
                }
                update_user_meta($user_id, 'mobile', $_POST['user_celular_register']);
                update_user_meta($user_id, 'tipo_usuario', $_POST['user_tipo_register']);
                update_user_meta($user_id, '_tipo_usuario', 'field_5f43400b89f97');
                wp_update_user(array(
                    'ID' => $user_id,
                    'first_name' => $_POST['user_name_register']
                ));
             }
             
        } else {
           print esc_html__('Email already exists.  Please choose a new one.','wpresidence');
        }
        die(); 
              
}

function wpestate_ajax_update_profile(){
        check_ajax_referer( 'wpestate_update_profile_nonce', 'security' );
        $current_user           =   wp_get_current_user();
        $userID                 =   $current_user->ID;
        $user_login             =   $current_user->user_login;
        check_ajax_referer( 'profile_ajax_nonce', 'security-profile' );
        if ( !is_user_logged_in() ) {   
            exit('ko');
        }
        if($userID === 0 ){
            exit('out pls');
        }


        
        $allowed_html               =   array('</br>');
        $firstname                  =   sanitize_text_field ( wp_kses( $_POST['firstname'] ,$allowed_html) );
        $secondname                 =   sanitize_text_field ( wp_kses( $_POST['secondname'] ,$allowed_html) );
        $useremail                  =   sanitize_text_field ( wp_kses( $_POST['useremail'] ,$allowed_html) );
        $userphone                  =   sanitize_text_field ( wp_kses( $_POST['userphone'] ,$allowed_html) );
        $usermobile                 =   sanitize_text_field ( wp_kses( $_POST['usermobile'] ,$allowed_html) );
        $userskype                  =   sanitize_text_field ( wp_kses( $_POST['userskype'] ,$allowed_html) );
        $usertitle                  =   sanitize_text_field ( wp_kses( $_POST['usertitle'] ,$allowed_html) );
        $about_me                   =   wp_kses( $_POST['description'],$allowed_html );
        $profile_image_url_small    =   sanitize_text_field ( wp_kses($_POST['profile_image_url_small'],$allowed_html) );
        $profile_image_url          =   sanitize_text_field ( wp_kses($_POST['profile_image_url'],$allowed_html) );       
        $whatsapp                   =   sanitize_text_field ( wp_kses( $_POST['whatsapp'],$allowed_html) );
        $userfacebook               =   sanitize_text_field ( wp_kses( $_POST['userfacebook'],$allowed_html) );
        $usertwitter                =   sanitize_text_field ( wp_kses( $_POST['usertwitter'],$allowed_html) );
        $userlinkedin               =   sanitize_text_field ( wp_kses( $_POST['userlinkedin'],$allowed_html) );
        $userpinterest              =   sanitize_text_field ( wp_kses( $_POST['userpinterest'],$allowed_html ) );
        $userinstagram              =   sanitize_text_field ( wp_kses( $_POST['userinstagram'],$allowed_html ) );
        $userurl                    =   sanitize_text_field ( wp_kses( $_POST['userurl'],$allowed_html ) );
        $agent_category_submit      =   sanitize_text_field ( wp_kses( $_POST['agent_category_submit'],$allowed_html ) );
        $agent_action_submit        =   sanitize_text_field ( wp_kses( $_POST['agent_action_submit'],$allowed_html ) );
        $agent_city                 =   sanitize_text_field ( wp_kses( $_POST['agent_city'],$allowed_html ) );
        $agent_county               =   sanitize_text_field ( wp_kses( $_POST['agent_county'],$allowed_html ) );
        $agent_area                 =   sanitize_text_field ( wp_kses( $_POST['agent_area'],$allowed_html ) );     
        $agent_member               =   sanitize_text_field ( wp_kses( $_POST['agent_member'],$allowed_html ) );   
        
		$agent_custom_label          =   $_POST['agent_custom_label'];
		$agent_custom_value          =   $_POST['agent_custom_value'];
	 
		// prcess fields data
		$agent_fields_array = array();
		for( $i=1; $i<count( $agent_custom_label  ); $i++ ){
			$agent_fields_array[] = array( 'label' => sanitize_text_field( $agent_custom_label[$i] ), 'value' => sanitize_text_field( $agent_custom_value[$i] ) );
		}
		
		
		
        update_user_meta( $userID, 'first_name', $firstname ) ;
        update_user_meta( $userID, 'last_name',  $secondname) ;
        update_user_meta( $userID, 'phone' , $userphone) ;
        update_user_meta( $userID, 'skype' , $userskype) ;
        update_user_meta( $userID, 'title', $usertitle) ;
        update_user_meta( $userID, 'custom_picture',$profile_image_url);
        update_user_meta( $userID, 'small_custom_picture',$profile_image_url_small);     
        update_user_meta( $userID, 'mobile' , $usermobile) ;
        
        
        
        update_user_meta( $userID, 'whatsapp' , $whatsapp) ;
        update_user_meta( $userID, 'facebook' , $userfacebook) ;
        update_user_meta( $userID, 'twitter' , $usertwitter) ;
        update_user_meta( $userID, 'linkedin' , $userlinkedin) ;
        update_user_meta( $userID, 'pinterest' , $userpinterest) ;
        update_user_meta( $userID, 'instagram' , $userinstagram) ;
        update_user_meta( $userID, 'description' , $about_me) ;
        update_user_meta( $userID, 'website' , $userurl) ;
        
        
        
        $agent_id=get_user_meta( $userID, 'user_agent_id',true);
        update_post_meta( $agent_id, 'agent_custom_data' , $agent_fields_array) ;
	 
      
                
        wpestate_update_user_agent ($agent_member,$agent_category_submit,$agent_action_submit,$agent_city,$agent_county,$agent_area,$userurl,$agent_id, $firstname ,$secondname ,$useremail,$userphone,$userskype,$usertitle,$profile_image_url,$usermobile,$about_me,$profile_image_url_small,$userfacebook,$usertwitter,$userlinkedin,$userpinterest,$userinstagram) ;
       
        
        if( $current_user->user_email != $useremail ) {
            $user_id=email_exists( $useremail ) ;
            if ( $user_id){
                esc_html_e('The email was not saved because it is used by another user.','wpresidence');
            } else{
                $args = array(
                    'ID'         => $userID,
                    'user_email' => $useremail
                ); 
                wp_update_user( $args );
            } 
        }
        
        $arguments=array(
            'user_profile'      =>  $user_login,
        );

        wpestate_select_email_type(get_option('admin_email'),'agent_update_profile',$arguments);
        esc_html_e('Profile updated','wpresidence');
        die(); 
   }

function c7_admin_head() {
    /*$user_query = new WP_User_Query(array(
        'role__in' => array('administrator', 'subscriber', 'shop_manager', 'customer', 'author', 'editor')
    ));
    if ( ! empty( $user_query->get_results() ) ) {
        $c = 0;
        foreach ( $user_query->get_results() as $user ) {
            $c++;
    if ($c < 5) {
            update_user_meta($user->ID, 'tipo_usuario', 'Persona Natural');
    } else {
            update_user_meta($user->ID, 'tipo_usuario', 'Agente Inmobiliario');
    }
            update_user_meta($user->ID, '_tipo_usuario', 'field_5f43400b89f97');
            echo '<p>' . $user->display_name . '</p>';
        }
    } else {
        echo 'No users found.';
    }die;*/
    $user = wp_get_current_user();
    if ( in_array( 'madretierra', (array) $user->roles ) ) {
 ?>
        <style>
            #adminmenu > li {
                display: none;
            }
            #adminmenu > li#menu-dashboard-posts,
            #adminmenu > li#menu-posts,
            #adminmenu > li#menu-media,
            #adminmenu > li#toplevel_page_reportes-diario,
            #adminmenu > li#toplevel_page_wpestate-crm,
            #adminmenu > li#menu-pages,
            #adminmenu > li#menu-posts-wpestate_search,
            #adminmenu > li#menu-posts-estate_agent,
            #adminmenu > li#menu-posts-estate_agency,
            #adminmenu > li#menu-posts-estate_developer,
            #adminmenu > li#menu-posts-estate_property,
            #adminmenu > li#menu-posts-wpestate_message,
            #adminmenu > li#menu-posts-contacto,
            #adminmenu > li#toplevel_page_elementor,
            #adminmenu > li#toplevel_page_edit-post_type-acf-field-group {
                display: block;
            }
        </style>
    <?php }
}
add_action('admin_head', 'c7_admin_head');

add_filter( 'parse_query', 'c7_admin_posts_filter' );
function c7_admin_posts_filter( $query )
{
    global $pagenow;

    if ( is_admin() && $pagenow=='edit.php' && $_GET['post_type']=='estate_property' && isset($_GET['property_tipo_usuario']) && $_GET['property_tipo_usuario'] != '') {
        $autores_ids = array();
        $user_query = new WP_User_Query(array(
            'role__in' => array('administrator', 'subscriber', 'shop_manager', 'customer', 'author', 'editor'),
            'meta_key' => 'tipo_usuario',
            'meta_value' => $_GET['property_tipo_usuario']
        ));
        if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                array_push($autores_ids, $user->ID);
            }
        }
        $query->query_vars['author__in'] = $autores_ids;
        return $query;
    }
}

add_action( 'restrict_manage_posts', 'c7_admin_posts_filter_restrict_manage_posts' );
function c7_admin_posts_filter_restrict_manage_posts() {
    global $pagenow;
    if ( is_admin() && $pagenow=='edit.php' && $_GET['post_type']=='estate_property') { ?>
        <select name="property_tipo_usuario">
        <option value=""><?php _e('Show all Users', 'madretierra'); ?></option>
        <option value="Persona Natural" <?php if(!empty($_GET['property_tipo_usuario']) && $_GET['property_tipo_usuario']=='Persona Natural') echo 'selected'; ?>>Persona Natural</option>
        <option value="Agente Inmobiliario" <?php if(!empty($_GET['property_tipo_usuario']) && $_GET['property_tipo_usuario']=='Agente Inmobiliario') echo 'selected'; ?>>Agente Inmobiliario</option>
    </select>
<?php }
}

function menu_admin_reportes() {
    add_menu_page(__( 'Reportes', 'madretierra' ), __( 'Reportes', 'madretierra' ), 'manage_options', 'reportes-diario',  'menu_admin_reportes_page_diario', 'dashicons-chart-bar', 16);
    add_submenu_page( 'reportes-diario', __( 'Reportes diario', 'madretierra' ), __( 'Reportes diario', 'madretierra' ), 'manage_options', 'reportes-diario', 'menu_admin_reportes_page_diario');
    add_submenu_page( 'reportes-diario', __( 'Reportes mensual', 'madretierra' ), __( 'Reportes mensual', 'madretierra' ), 'manage_options', 'reporte-mensual', 'menu_admin_reportes_page_mensual');
}
add_action( 'admin_menu', 'menu_admin_reportes' );

function menu_admin_reportes_page_diario() {
    ob_start();
    include 'admin-reportes.php';
    $reportes = ob_get_clean();
    echo $reportes;
}

function menu_admin_reportes_page_mensual() {
    ob_start();
    include 'admin-reportes-mensual.php';
    $reportes = ob_get_clean();
    echo $reportes;
}

add_filter('woocommerce_add_cart_item_data','o612_add_item_data',10,3);
function o612_add_item_data($cart_item_data, $product_id, $variation_id) {

    if(isset($_REQUEST['propiedad'])) {
        $cart_item_data['propiedad'] = $_REQUEST['propiedad'];
    }
    if(isset($_REQUEST['donacion'])) {
        $cart_item_data['donacion'] = $_REQUEST['donacion'];
    }
    return $cart_item_data;
}

add_filter('woocommerce_get_item_data','o612_add_item_meta',10,2);
function o612_add_item_meta($item_data, $cart_item) {

    if(array_key_exists('propiedad', $cart_item)) {
        $item_data[] = array(
            'key'   => 'propiedad',
            'value' => get_the_title($cart_item['propiedad'])
        );
    }
    if(array_key_exists('donacion', $cart_item)) {
        $item_data[] = array(
            'key'   => 'donacion',
            'value' => '$'.$cart_item['donacion']
        );
    }
    return $item_data;
}

add_action( 'woocommerce_checkout_create_order_line_item', 'o612_add_custom_order_line_item_meta',10,4 );
function o612_add_custom_order_line_item_meta($item, $cart_item_key, $values, $order) {

    if(array_key_exists('propiedad', $values)) {
        $item->add_meta_data('propiedad', get_the_title($cart_item['propiedad']));
    }
    if(array_key_exists('donacion', $values)) {
        $item->add_meta_data('donacion', '$'.$cart_item['donacion']);
    }
}

add_action( 'woocommerce_before_calculate_totals', 'add_custom_price', 20, 1);
function add_custom_price( $cart ) {

    // This is necessary for WC 3.0+
    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    // Avoiding hook repetition (when using price calculations for example)
    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
        return;

    // Loop through cart items
    foreach ( $cart->get_cart() as $item ) {
        if (!empty($item['donacion'])) {
            $incremento = $item['donacion'];
            $item['data']->set_price( $incremento );
        }
    }
}

add_action('admin_footer', 'c7_admin_footer');
function c7_admin_footer() { ?>
    <script>
        jQuery(function($) {
            $('#menu-posts-estate_property a[href="edit-tags.php?taxonomy=property_city&post_type=estate_property"]').text('Provincia')
            $('#menu-posts-estate_property a[href="edit-tags.php?taxonomy=property_area&post_type=estate_property"]').text('Distrito')
            $('#menu-posts-estate_property a[href="edit-tags.php?taxonomy=property_county_state&post_type=estate_property"]').text('Ciudad')
        })
    </script>
<?php }