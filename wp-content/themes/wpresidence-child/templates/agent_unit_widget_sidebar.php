<?php
global $wpestate_options;
global $agent_wid;
if($agent_wid!=0){
    $thumb_id           = get_post_thumbnail_id($agent_wid);
    $preview            = wp_get_attachment_image_src(get_post_thumbnail_id($agent_wid), 'agent_picture_thumb');
    $name               = get_the_title($agent_wid);
    $link               = esc_url( get_permalink($agent_wid) );
    
    
    $extra= array(
            'data-original'=>$preview[0],
            'class'	=> 'lazyload img-responsive',    
            );
    $thumb_prop    = get_the_post_thumbnail_url($agent_wid, 'agent_picture_thumb',$extra);

    $agent_posit        = esc_html( get_post_meta($agent_wid, 'agent_position', true) );
    $type=get_post_type($agent_wid);
     
    if( $type=='estate_agent' ){
        $agent_mobile       = esc_html( get_post_meta($agent_wid, 'agent_mobile', true) );
        $agent_email        = esc_html( get_post_meta($agent_wid, 'agent_email', true) );

    }else if( $type=='estate_agency' ){
        $agent_mobile       = esc_html( get_post_meta($agent_wid, 'agency_mobile', true) );
        $agent_email        = esc_html( get_post_meta($agent_wid, 'agency_email', true) );

    }else if($type=='estate_developer'){
        $agent_mobile       = esc_html( get_post_meta($agent_wid, 'developer_mobile', true) );
        $agent_email        = esc_html( get_post_meta($agent_wid, 'developer_email', true) );

      
    }
    
    
}else{
     $agent_wid=    get_post_field( 'post_author', $post->ID );
    
    $thumb_prop    =   get_the_author_meta( 'custom_picture',$agent_wid  );
    if($thumb_prop==''){
        $thumb_prop=get_theme_file_uri('/img/default-user.png');
    }
    
  
    $agent_mobile   = get_the_author_meta( 'mobile'  ,$agent_wid);
    $agent_pitch    = '';
    $agent_posit    = get_the_author_meta( 'title' ,$agent_wid ); 
    $agent_whatsapp = get_user_meta($agent_wid, 'whatsapp', true);
    $link           =  esc_url( get_permalink() );
    $name           =  get_the_author_meta( 'first_name' ).' '.get_the_author_meta( 'last_name');
    
}


$counter            =   count_user_posts($agent_wid,'estate_property',true);
$user_for_id = intval(get_post_meta($agent_wid,'user_meda_id',true));
if($user_for_id!=0){
$counter            =   count_user_posts($user_for_id,'estate_property',true);
}


$col_class=4;
if($wpestate_options['content_class']=='col-md-12'){
    $col_class=3;
}
           
?>




    <!--<div class="agent_unit_widget_sidebar" style="background-image: url(<?php //*echo esc_url($thumb_prop);?>)"></div>
    <h4> <a href="<?php //*echo esc_url($link); ?>"><?php //*echo esc_html($name); ?></a></h4>
    <div class="agent_position"><?php //*echo esc_html($agent_posit); ?></div>
    <div class="agent_listings"><?php  //*echo esc_html($counter).' '.esc_html__('listings','wpresidence');?></div>-->
    <div class="agent_sidebar_mobile">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xml:space="preserve" version="1.1" style="shape-rendering:geometricPrecision;text-rendering:geometricPrecision;image-rendering:optimizeQuality;" viewBox="0 0 295.64 369.5375" x="0px" y="0px" fill-rule="evenodd" clip-rule="evenodd"><defs></defs><g><path class="fil0" d="M231.99 189.12c18.12,10.07 36.25,20.14 54.37,30.21 7.8,4.33 11.22,13.52 8.15,21.9 -15.59,42.59 -61.25,65.07 -104.21,49.39 -87.97,-32.11 -153.18,-97.32 -185.29,-185.29 -15.68,-42.96 6.8,-88.62 49.39,-104.21 8.38,-3.07 17.57,0.35 21.91,8.15 10.06,18.12 20.13,36.25 30.2,54.37 4.72,8.5 3.61,18.59 -2.85,25.85 -8.46,9.52 -16.92,19.04 -25.38,28.55 18.06,43.98 55.33,81.25 99.31,99.31 9.51,-8.46 19.03,-16.92 28.55,-25.38 7.27,-6.46 17.35,-7.57 25.85,-2.85z"/></g></svg>
        <a href="tel:><?php echo esc_html($agent_mobile);?>" ><?php echo esc_html($agent_mobile);?></a>
        <?php if($agent_whatsapp) { ?>
            &nbsp; <a href="https://wa.me/<?php echo esc_html($agent_whatsapp);?>?text=<?php the_permalink(); ?>" class="agente-whatsapp"><i class="fa fa-whatsapp"></i> <?php echo esc_html($agent_whatsapp);?></a>
        <?php } ?>
    </div>
    
<!-- </div>    -->