<?php 
global $post;
global $adv_search_type;
$adv_search_what            =   wpresidence_get_option('wp_estate_adv_search_what','');
$show_adv_search_visible    =   wpresidence_get_option('wp_estate_show_adv_search_visible','');
$close_class                =   '';

if($show_adv_search_visible=='no'){
    $close_class='adv-search-1-close';
}

$extended_search    =   wpresidence_get_option('wp_estate_show_adv_search_extended','');
$extended_class     =   '';

if ($adv_search_type==2){
     $extended_class='adv_extended_class2';
}

if ( $extended_search =='yes' ){
    $extended_class='adv_extended_class';
    if($show_adv_search_visible=='no'){
        $close_class='adv-search-1-close-extended';
    }
       
}
$adv6_taxonomy          =   wpresidence_get_option('wp_estate_adv6_taxonomy');
$adv6_taxonomy_terms    =   wpresidence_get_option('wp_estate_adv6_taxonomy_terms');     
$adv6_max_price         =   wpresidence_get_option('wp_estate_adv6_max_price');     
$adv6_min_price         =   wpresidence_get_option('wp_estate_adv6_min_price');     
$allowed_html           =   array();
?>
<?php if(is_page(array(20206))){ ?>
<div class="titulo-buscador">
    <h2>BUSCAR PROPIEDADES</h2>
    <p class="single-content"><?php _e('El siguiente cuadro de búsqueda te permitirá buscar según el requerimiento que tengas, selecciona ya sea por categoría como casas, departamentos, oficinas, locales, terrenos, etc. Luego puedes filtrar la ubicación y especificar la ciudad, provincia y distrito, asi como el área y precio.', 'madretierra'); ?></p>
</div>
<div class="adv-search-1 <?php echo esc_attr($close_class.' '.$extended_class);?>" id="adv-search-8" > 

        <?php
        if (function_exists('icl_translate') ){
            print do_action( 'wpml_add_language_form_field' );
        }
        ?>   

        <div class="adv8-holder">
            <?php
            $custom_advanced_search         =   wpresidence_get_option('wp_estate_custom_advanced_search','');
            $adv_search_fields_no_per_row   =   ( floatval( wpresidence_get_option('wp_estate_search_fields_no_per_row') ) );
                        print '<div role="tabpanel" class="adv_search_tab '.wpestate_search_tab_align().' " id="tab_prpg_adv6">';
                    
                        $tab_items      =   '';
                        $tab_content    =   '';
                        $active         =   'active';
                        if(isset($_GET['adv6_search_tab']) && $_GET['adv6_search_tab']!=''){
                            $active         =   '';
                        }

                        $tipos = get_terms(array(
                            'taxonomy' => 'property_action_category',
                            'hide_empty' => false
                        ));
                        $adv6_taxonomy_terms = array();
                        if ($tipos) {
                            foreach ($tipos as $tipo) {
                                array_push($adv6_taxonomy_terms, $tipo->term_id);
                            }
                        }
                        $categorias = get_terms(array(
                            'taxonomy' => 'property_category',
                            'hide_empty' => false
                        ));
                        $categ_select_list = '';
                        if ($categorias) {
                            foreach ($categorias as $categoria) {
                                $categ_select_list .= '<li role="presentation" data-value="'.$categoria->slug.'">'.$categoria->name.'</li>';
                            }
                        }
                        $ciudades = get_terms(array(
                            'taxonomy' => 'property_county_state',
                            'hide_empty' => false
                        ));
                        $ciudades_select_list = '';
                        if ($ciudades) {
                            foreach ($ciudades as $ciudad) {
                                $ciudades_select_list .= '<li role="presentation" data-value="'.$ciudad->slug.'" data-name="'.$ciudad->name.'">'.$ciudad->name.'</li>';
                            }
                        }
                        $provincias = get_terms(array(
                            'taxonomy' => 'property_city',
                            'hide_empty' => false
                        ));
                        $provincias_select_list = '';
                        if ($provincias) {
                            foreach ($provincias as $provincia) {
                                $stateparent = '';
                                $term_meta = get_option( "taxonomy_$provincia->term_id");
                                if ($term_meta) $stateparent = $term_meta['stateparent'];
                                $provincias_select_list .= '<li role="presentation" data-value="'.$provincia->slug.'" data-name="'.$provincia->name.'" data-stateparent="'.$stateparent.'" style="display:none;">'.$provincia->name.'</li>';
                            }
                        }
                        $distritos = get_terms(array(
                            'taxonomy' => 'property_area',
                            'hide_empty' => false
                        ));
                        $distritos_select_list = '';
                        if ($distritos) {
                            foreach ($distritos as $distrito) {
                                $cityparent = '';
                                $term_meta = get_option( "taxonomy_$distrito->term_id");
                                if ($term_meta) $cityparent = $term_meta['cityparent'];
                                $distritos_select_list .= '<li role="presentation" data-value="'.$distrito->slug.'" data-cityparent="'.$cityparent.'" style="display:none;">'.$distrito->name.'</li>';
                            }
                        }

                        if(is_array($adv6_taxonomy_terms)){
                            foreach ($adv6_taxonomy_terms as $term_id){
                                $term               =   get_term( $term_id, $adv6_taxonomy);
                                $use_name           =   sanitize_title($term->name);
                                $use_title_name     =   $term->name;


                                if(isset($_GET['adv6_search_tab']) && $_GET['adv6_search_tab']==$use_name){
                                    $active         =   'active';
                                }

                                $tab_items.= '<div class="adv_search_tab_item '.esc_attr($active).' '.esc_attr($use_name).'" data-term="'.esc_attr($use_name).'" data-termid="'.esc_attr($term_id).'" data-tax="'.esc_attr($adv6_taxonomy).'">
                                <a href="#'.urldecode($use_name).'" aria-controls="'.urldecode($use_name).'" role="tab" class="adv6_tab_head" data-toggle="tab">'.urldecode (str_replace("-"," ",$use_title_name)).'</a>
                                </div>';

                                if(pll_current_language() == 'en') {
                                    $action = 'en/search';
                                } else {
                                    $action = 'buscador';
                                }
                                $tab_content.='
                                <div role="tabpanel" class="tab-pane '.esc_attr($active).'" id="'.urldecode($use_name).'">
                                    <form  role="search" method="get" action="'.esc_url(home_url($action)).'" >';

                                        if($adv6_taxonomy=='property_category'){
                                            $tab_content.='<input type="hidden" class="picked_tax" name="filter_search_type[]" value="'.esc_attr($use_name).'" >';
                                        }else if($adv6_taxonomy=='property_action_category'){
                                            $tab_content.='<input type="hidden" class="picked_tax" name="filter_search_action[]" value="'.esc_attr($use_name).'" >';
                                        }else if($adv6_taxonomy=='property_city'){
                                            $tab_content.='<input type="hidden" class="picked_tax" name="advanced_city" value="'.esc_attr($use_name).'" >';
                                        }else if($adv6_taxonomy=='property_area'){
                                            $tab_content.='<input type="hidden" class="picked_tax" name="advanced_area" value="'.esc_attr($use_name).'" >';
                                        }else if($adv6_taxonomy=='property_county_state'){
                                            $tab_content.='<input type="hidden" class="picked_tax" name="advanced_contystate" value="'.esc_attr($use_name).'" >';
                                        }


                                        $tab_content.='<input type="hidden" name="adv6_search_tab" value="'.esc_attr($use_name).'">
                                        <input type="hidden" name="term_id" value="'.esc_html($term_id).'">'; 


                                        if (function_exists('icl_translate') ){
                                            $tab_content.= do_action( 'wpml_add_language_form_field' );
                                        }

                                        $tab_content.='<div class="col-md-4">';

                                                if($adv6_taxonomy!=='property_category'){

                                                    $tab_content.='<div class="dropdown form-control" >
                                                            <div data-toggle="dropdown" id="adv_categ" class="filter_menu_trigger" data-value="">';

                                                            if ( isset($_GET['filter_search_type'][0]) && trim($_GET['filter_search_type'][0])!=''  && trim($_GET['filter_search_type'][0])!='all' ){
                                                               // $tab_content.= ucwords ( str_replace("-"," ",  esc_attr( wp_kses( rawurldecode   ( stripslashes( $_GET['filter_search_type'][0] ) ), $allowed_html) ) ) );
                                                                $full_name   =  get_term_by('slug', ( ( $_GET['filter_search_type'][0] ) ),'property_category');
                                                                $tab_content.=  $full_name->name;
                                                            }else{
                                                                $tab_content.= esc_html__('Categories','wpresidence'); 
                                                            }   

                                                            $tab_content.= '<span class="caret caret_filter"></span> </div>   

                                                            <input type="hidden" name="filter_search_type[]" value="';

                                                                if(isset($_GET['filter_search_type'][0])){
                                                                    $tab_content.= ucwords ( str_replace("-"," ",  esc_attr( wp_kses($_GET['filter_search_type'][0], $allowed_html) ) ) );

                                                                    } 

                                                            $tab_content.='    ">

                                                            <ul  class="dropdown-menu filter_menu" role="menu" aria-labelledby="adv_categ">
                                                                '.$categ_select_list.'
                                                            </ul>
                                                    </div>';
                                                }

                                                if($adv6_taxonomy!=='property_action_category'){

                                                    $tab_content.='<div class="dropdown form-control" >
                                                            <div data-toggle="dropdown" id="adv_actions" class="filter_menu_trigger" data-value=""> ';

                                                            if( isset($_GET['filter_search_action'][0])  && trim($_GET['filter_search_action'][0])!='' && trim($_GET['filter_search_action'][0])!='all'){
                                                                //$tab_content.= ucwords ( str_replace("-"," ",esc_attr( wp_kses(   rawurldecode ( stripslashes(  $_GET['filter_search_action'][0]) ), $allowed_html) ) ) );
                                                                $full_name   =  get_term_by('slug', ( ( $_GET['filter_search_action'][0] ) ),'property_action_category');
                                                                $tab_content.=  $full_name->name;

                                                            }else{
                                                                $tab_content.= esc_html__('Types','wpresidence'); 
                                                            }

                                                            $tab_content.='<span class="caret caret_filter"></span> </div>           

                                                            <input type="hidden" name="filter_search_action[]" value="';

                                                            if(isset($_GET['filter_search_action'][0])){
                                                                $tab_content.= ucwords ( str_replace("-"," ", esc_attr( wp_kses($_GET['filter_search_action'][0], $allowed_html) ) ) );

                                                            }

                                                            $tab_content.='">
                                                            <ul  class="dropdown-menu filter_menu" role="menu" aria-labelledby="adv_actions">
                                                                '.$action_select_list.'
                                                            </ul>
                                                    </div>';   
                                                }
                                            $tab_content.='</div>
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="dropdown form-control" >
                                                            <div data-toggle="dropdown" id="adv_categ" class="filter_menu_trigger" data-value="">Ciudad<span class="caret caret_filter"></span> </div>   

                                                            <input type="hidden" name="filter_ciudades[]" value="    ">

                                                            <ul class="dropdown-menu filter_menu" id="ciudades_select_list" role="menu" aria-labelledby="adv_categ">'.$ciudades_select_list.'</ul>        
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="dropdown form-control" >
                                                            <div data-toggle="dropdown" id="adv_categ" class="filter_menu_trigger" data-value="">Provincia<span class="caret caret_filter"></span> </div>   

                                                            <input type="hidden" name="filter_provincias[]" value="    ">

                                                            <ul class="dropdown-menu filter_menu" id="provincias_select_list" role="menu" aria-labelledby="adv_categ">'.$provincias_select_list.'</ul>        
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="dropdown form-control" >
                                                            <div data-toggle="dropdown" id="adv_categ" class="filter_menu_trigger" data-value="">Distrito<span class="caret caret_filter"></span> </div>   

                                                            <input type="hidden" name="filter_distritos[]" value="    ">

                                                            <ul class="dropdown-menu filter_menu" id="distritos_select_list" role="menu" aria-labelledby="adv_categ">'.$distritos_select_list.'</ul>        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="row" style="margin: 0 -15px !important;">
                                                    <div class="col-md-2 adv2_nopadding" style="padding: 12px 0 0 25px !important;">
													<span>Área:</span>
														</div>
                                                    <div class="col-md-5 adv2_nopadding" style="padding: 0 0 0 15px !important;">
                                                        <input type="text" id="adv_location" class="form-control adv_locations_search" name="area_desde"  placeholder="'. esc_html__('Desde m²','madretierra').'" value="">      
                                                    </div>
                                                    <div class="col-md-5 adv2_nopadding" style="padding: 0 15px 0 15px !important;">
                                                        <input type="text" id="adv_location" class="form-control adv_locations_search" name="area_hasta"  placeholder="'. esc_html__('Hasta m²','madretierra').'" value="">      
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="col-md-4">
                                                <div class="row" style="margin: 0 -15px !important;">
                                                    <div class="col-md-2 adv2_nopadding" style="padding: 12px 0 0 15px !important;">
													<span>Precio:</span>
													</div>
                                                    <div class="col-md-5 adv2_nopadding" style="padding: 0 0 0 15px !important;">
                                                        <input type="text" id="adv_location" class="form-control adv_locations_search" name="precio_desde"  placeholder="'. esc_html__('Desde: USD','madretierra').'" value="">      
                                                    </div>
                                                    <div class="col-md-5 adv2_nopadding" style="padding: 0 15px 0 15px !important;">
                                                        <input type="text" id="adv_location" class="form-control adv_locations_search" name="precio_hasta"  placeholder="'. esc_html__('Hasta: USD','madretierra').'" value="">      
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 boton-bus">
                                                <input type="hidden" name="is2" value="1">
                                                <input name="submit" type="submit" class="wpresidence_button" id="advanced_submit_22" value="'.esc_html__('Search Properties','wpresidence').'">
                                            </div>';

                                        if($extended_search=='yes'){
                                            ob_start();
                                            show_extended_search('adv');
                                            $tab_content.=ob_get_contents();
                                            ob_end_clean();
                                        }    

                                    $tab_content.='<input type="hidden" name="adv6_search_tab" value="'.esc_attr($use_name).'">
                                    <input type="hidden" name="term_id" value="'.esc_attr($term_id).'">';    

                                    $tab_content.='</form>        
                                </div>  ';
                                $active='';
                            }
                        }
                
                    print '<div class="nav nav-tabs" role="tablist">'.$tab_items.'</div>'; //escaped above    
                    print '<div class="tab-content">'.$tab_content.'</div>'; // escpaed above
                    print'</div>';
   
            ?>
            
            <?php include( locate_template('templates/preview_template.php') ); ?>
        </div>
       <div style="clear:both;"></div>
</div>
<?php } ?>

<?php
$availableTags='';
$args = array(
    'orderby' => 'count',
    'hide_empty' => 0,
); 

$terms = get_terms( 'property_city', $args );
foreach ( $terms as $term ) {
   $availableTags.= '"'.esc_html($term->name).'",';
}

$terms = get_terms( 'property_area', $args );

foreach ( $terms as $term ) {
   $availableTags.= '"'.esc_html($term->name).'",';
}

$terms = get_terms( 'property_county_state', $args );
foreach ( $terms as $term ) {
   $availableTags.= '"'.esc_html($term->name).'",';
}
//$availableTags is escaped above
 print '<script type="text/javascript">
                       //<![CDATA[
                       jQuery(document).ready(function(){
                            var availableTags = ['.$availableTags.'];
                            jQuery(".adv_locations_search").autocomplete({
                                source: availableTags,
                                change: function() {
                                    wpestate_show_pins();
                                }
                            });
                       });
                       //]]>
                       </script>';
 
?>
