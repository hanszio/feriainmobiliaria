<?php
$uri_template = get_bloginfo('template_url');
$current_user               =   wp_get_current_user();
$user_custom_picture        =   get_the_author_meta( 'small_custom_picture' , $current_user->ID  );
$user_small_picture_id      =   get_the_author_meta( 'small_custom_picture' , $current_user->ID  );
if( $user_small_picture_id == '' ){
    $user_small_picture[0]= get_theme_file_uri('/img/default_user_small.png');
}else{
    $user_small_picture=wp_get_attachment_image_src($user_small_picture_id,'user_thumb');
}
?>
<?php
global $wpestate_global_payments;
$show_top_bar_user_login    =   esc_html ( wpresidence_get_option('wp_estate_show_top_bar_user_login','') );
?>

<?php if(is_user_logged_in()){ ?>

    <div class="user_menu user_loged" id="user_menu_u">
		
        <?php 
        echo wpestate_header_phone();
        
        if($show_top_bar_user_login=='yes'){ ?>
            <?php if(  class_exists( 'WooCommerce' ) ){
                $wpestate_global_payments->show_cart_icon();
            }
            ?>
            <a class="menu_user_tools dropdown" id="user_menu_trigger" data-toggle="dropdown"> 
                <a class="navicon-button x">
                    <div class="navicon"></div>
                </a>
            <div class="menu_user_picture" style="background-image: url('<?php print esc_attr($user_small_picture[0]); ?>');"></div>
        <?php } ?>
    </div> 
<?php }else{ ?>
	
    <div class="user_menu user_not_loged" id="user_menu_u">

        <?php
        
        if($show_top_bar_user_login=='yes'){
            if(  class_exists( 'WooCommerce' ) ){
                $wpestate_global_payments->show_cart_icon();
            }
        ?>
        
            <a class="menu_user_tools dropdown" id="user_menu_trigger" data-toggle="dropdown">  
                <a class="navicon-button nav-notlog x">
                    <div class="navicon"></div>
                </a>
        
       <?php
        }
        
        echo wpestate_header_phone(); 
        ?>
    </div>   
<?php } ?>   
                  
 
        
        
<?php 
if ( 0 != $current_user->ID  && is_user_logged_in() ) {
    $username               =   $current_user->user_login ;
    ?> 
    <ul id="user_menu_open" class="dropdown-menu menulist topmenux" role="menu" aria-labelledby="user_menu_trigger"> 
        <?php wpestate_generate_user_menu('top'); ?>
    </ul>
<?php }?>

        
<?php 
    if(  class_exists( 'WooCommerce' ) ){
        $wpestate_global_payments->show_cart();
    }
?>