<?php
$link           =   esc_url( get_permalink() );

$property_size = wpestate_get_converted_measure( $post->ID, 'property_size' );
$antiguedad = get_post_meta($post->ID, 'antiguedad-de-la-construccion', true) ; ?>

<div class="property_listing_details">
    <?php 

        if($property_size!=''){
            print '<span class="infosize">
            <svg  viewBox="0 0 42 32" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M41 0H13C12.45 0 12 0.45 12 1V10H1C0.45 10 0 10.45 0 11V31C0 31.55 0.45 32 1 32H29C29.55 32 30 31.55 30 31V22H41C41.55 22 42 21.55 42 21V1C42 0.45 41.55 0 41 0ZM28 30H2V12H28V30ZM40 20H30V11C30 10.45 29.55 10 29 10H14V2H40V20Z" fill="black"/>
</svg>'     .($property_size).'</span>';
        }

        if($antiguedad!=''){
            print '<span class="antiguedad">
            <i class="fa fa-clock-o"></i> Hace ' . human_time_diff( date('U', strtotime($antiguedad.'-01-01')), current_time('timestamp') ).'</span>';
        }

    print wpestate_share_unit_desing($post->ID); ?>
    <span class="share_list" style="float:right;" data-original-title="<?php esc_attr_e('share','wpresidence');?>" ></span>
    <?php echo '<a href="'.esc_url($link).'" target="'.esc_attr(wpresidence_get_option('wp_estate_unit_card_new_page','')).'"  class="unit_details_x">'.esc_html__('details','wpresidence').'</a>'; ?>
    <?php $post_author_id = get_post_field( 'post_author', $post->ID );
    if ($post_author_id == 1) { ?>
        <?php echo '<a href="'.home_url('carrito').'?add-to-cart=29205&propiedad='.$post->ID.'"  class="unit_details_x" style="margin-right:7px;">'.esc_html__('reservar','madretierra').'</a>'; ?>
    <?php } ?>

</div>