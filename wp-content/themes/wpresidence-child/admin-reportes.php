<?php $uri_template = get_bloginfo('template_url');
$colores = array('#056676', '#5eaaa8', '#a3d2ca', '#e8ded2', '#f1f1e8', '#bfdcae', '#81b214', '#206a5d', '#056676', '#5eaaa8', '#a3d2ca', '#e8ded2', '#f1f1e8', '#bfdcae', '#81b214', '#206a5d',
  '#056676', '#5eaaa8', '#a3d2ca', '#e8ded2', '#f1f1e8', '#bfdcae', '#81b214', '#206a5d', '#056676', '#5eaaa8', '#a3d2ca', '#e8ded2', '#f1f1e8', '#bfdcae', '#81b214', '#206a5d'
);
$datasets_propiedades_visitas = $datasets_categorias_visitas = $datasets_publicaciones_diarias = $datasets_redes_visitas = $labels_display = $labels = array();
$begin = new DateTime(date('Y-m-d', strtotime(date('Y-m-d')." -21 day")));
$end = new DateTime(date('Y-m-d', strtotime(date('Y-m-d')." +1 day")));
$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($begin, $interval, $end);
foreach ($period as $dt) {
    array_push($labels, $dt->format("Y-m-d"));
    array_push($labels_display, $dt->format("d/m"));
}

$propiedades_query = new WP_Query(array(
  'post_type' => 'estate_property',
  'posts_per_page' => -1
));
$c = 0;
while ($propiedades_query->have_posts()) { $propiedades_query->the_post();
  $visitas = get_post_meta(get_the_id(), 'visitas', true);
  $data = array();
  foreach ($labels as $label) {
    if (empty($visitas[$label])) {
      array_push($data, 0);
    } else {
      array_push($data, $visitas[$label]);
    }
  }
  array_push($datasets_propiedades_visitas, array(
		'label' => get_the_title(),
		'backgroundColor' => $colores[$c],
		'borderColor' => $colores[$c],
		'borderWidth' => 1,
		'data' => $data
  ));
  $c++;
}

$propiedades_category = get_terms(array(
  'taxonomy' => 'property_category',
  'hide_empty' => false
));
if ($propiedades_category) {
  $c = 0;
  foreach ($propiedades_category as $category) {
    $visitas = get_term_meta($category->term_id, 'visitas', true);
    $data = array();
    foreach ($labels as $label) {
      if (empty($visitas[$label])) {
        array_push($data, 0);
      } else {
        array_push($data, $visitas[$label]);
      }
    }
    array_push($datasets_categorias_visitas, array(
      'label' => $category->name,
      'backgroundColor' => $colores[$c],
      'borderColor' => $colores[$c],
      'borderWidth' => 1,
      'data' => $data
    ));
    $c++;
  }
}

$usuarios = array('admin'=>'Madre Tierra', 'usuarios'=>'Usuarios');
  $c = 0;
  foreach ($usuarios as $usuario => $nombre) {
    $visitas = get_option('visitas_'.$usuario);
    $data = array();
    foreach ($labels as $label) {
      if (empty($visitas[$label])) {
        array_push($data, 0);
      } else {
        array_push($data, $visitas[$label]);
      }
    }
    array_push($datasets_publicaciones_diarias, array(
      'label' => $nombre,
      'backgroundColor' => $colores[$c],
      'borderColor' => $colores[$c],
      'borderWidth' => 1,
      'data' => $data
    ));
    $c++;
  }

$redes = array('facebook', 'youtube', 'instagram', 'whatsapp');
  $c = 0;
  foreach ($redes as $red) {
    $visitas = get_option('visitas_'.$red);
    $data = array();
    foreach ($labels as $label) {
      if (empty($visitas[$label])) {
        array_push($data, 0);
      } else {
        array_push($data, $visitas[$label]);
      }
    }
    array_push($datasets_redes_visitas, array(
      'label' => ucfirst($red),
      'backgroundColor' => $colores[$c],
      'borderColor' => $colores[$c],
      'borderWidth' => 1,
      'data' => $data
    ));
    $c++;
  } ?>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.3/jspdf.min.js"></script>
<style media="screen">
  .visitas_propiedades,
  .visitas_categorias,
  .visitas_servicios,
  .visitas_redes {
    width: 49%;
    display: inline-block;
  }
  .wrap h1.wp-heading-inline {
    display: block;
  }
  #downloadPdf {
    position: absolute;
    right: 20px;
    top: 20px;
  }
</style>
<div class="wrap">
  <h1 class="wp-heading-inline">Reportes</h1>
  <button id="downloadPdf" class="button button-primary">Descargar</button>
  <div class="visitas_propiedades">
    <canvas id="visitasPropiedades"></canvas>
  </div>
  <div class="visitas_categorias">
    <canvas id="visitasCategorias"></canvas>
  </div>
  <div class="visitas_servicios">
    <canvas id="visitasServicios"></canvas>
  </div>
  <div class="visitas_redes">
    <canvas id="visitasRedes"></canvas>
  </div>
</div>
<script>
		var barChartData1 = {
			labels: <?php echo json_encode($labels_display); ?>,
			datasets: <?php echo json_encode($datasets_propiedades_visitas); ?>
		};

		var barChartData2 = {
			labels: <?php echo json_encode($labels_display); ?>,
			datasets: <?php echo json_encode($datasets_categorias_visitas); ?>
		};

		var barChartData3 = {
			labels: <?php echo json_encode($labels_display); ?>,
			datasets: <?php echo json_encode($datasets_publicaciones_diarias); ?>
		};

		var barChartData4 = {
			labels: <?php echo json_encode($labels_display); ?>,
			datasets: <?php echo json_encode($datasets_redes_visitas); ?>
		};

		window.onload = function() {
			var ctx1 = document.getElementById('visitasPropiedades').getContext('2d');
			window.myBar1 = new Chart(ctx1, {
				type: 'bar',
				data: barChartData1,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Visitas diarias por propiedades'
					},
          scales: {
            yAxes: [{
             ticks: {
              beginAtZero: true,
              userCallback: function(label, index, labels) {
                // when the floored value is the same as the value we have a whole number
                if (Math.floor(label) === label) {
                    return label;
                }
              },
             }
            }],
          }
				}
			});

			var ctx2 = document.getElementById('visitasCategorias').getContext('2d');
			window.myBar2 = new Chart(ctx2, {
				type: 'bar',
				data: barChartData2,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Visitas diarias por categoría'
					},
          scales: {
            yAxes: [{
             ticks: {
              beginAtZero: true,
              userCallback: function(label, index, labels) {
                // when the floored value is the same as the value we have a whole number
                if (Math.floor(label) === label) {
                    return label;
                }
              },
             }
            }],
          }
				}
			});

			var ctx3 = document.getElementById('visitasServicios').getContext('2d');
			window.myBar3 = new Chart(ctx3, {
				type: 'bar',
				data: barChartData3,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Publicaciones diarias por usuario'
					},
          scales: {
            yAxes: [{
             ticks: {
              beginAtZero: true,
              userCallback: function(label, index, labels) {
                // when the floored value is the same as the value we have a whole number
                if (Math.floor(label) === label) {
                    return label;
                }
              },
             }
            }],
          }
				}
			});

			var ctx4 = document.getElementById('visitasRedes').getContext('2d');
			window.myBar4 = new Chart(ctx4, {
				type: 'bar',
				data: barChartData4,
				options: {
					responsive: true,
					legend: {
						position: 'top',
					},
					title: {
						display: true,
						text: 'Visitas diarias por redes sociales'
					},
          scales: {
            yAxes: [{
             ticks: {
              beginAtZero: true,
              userCallback: function(label, index, labels) {
                // when the floored value is the same as the value we have a whole number
                if (Math.floor(label) === label) {
                    return label;
                }
              },
             }
            }],
          }
				}
			});

    jQuery('#downloadPdf').click(function(event) {
  // create a new canvas object that we will populate with all other canvas objects
  var pdfCanvas = jQuery('<canvas />').attr({
    id: "canvaspdf",
    width: 1600,
    height: 900
  });

  // keep track canvas position
  var pdfctx = jQuery(pdfCanvas)[0].getContext('2d');
  var pdfctxX = 0;
  var pdfctxY = 0;
  var buffer = 100;

  // for each chart.js chart
  jQuery("canvas").each(function(index) {
    // get the chart height/width
    var canvasHeight = jQuery(this).innerHeight();
    var canvasWidth = jQuery(this).innerWidth();

    // draw the chart into the new canvas
    pdfctx.drawImage(jQuery(this)[0], pdfctxX, pdfctxY, canvasWidth, canvasHeight);
    pdfctxX += canvasWidth + buffer;

    // our report page is in a grid pattern so replicate that in the new canvas
    if (index % 2 === 1) {
      pdfctxX = 0;
      pdfctxY += canvasHeight + buffer;
    }
  });

  // create new pdf and add our new canvas as an image
  var pdf = new jsPDF('l', 'pt', [1100, 600]);
  pdf.addImage(jQuery(pdfCanvas)[0], 'PNG', 0, 0);

  // download the pdf
  pdf.save('filename.pdf');
});

		};
</script>