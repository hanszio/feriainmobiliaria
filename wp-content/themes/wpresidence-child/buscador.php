<?php /* Template Name: Buscador */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
get_header();
?>

<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class=" col-md-9 rightmargin full_width_prop">
        <p>&nbsp;</p>
        <?php $filtros_buscados = $filter_taxs = $filter_metas = array();
        if (!empty($_GET['filter_search_action']) && $_GET['filter_search_action'][0]!='') {
            $current_tipo = get_term_by('slug', $_GET['filter_search_action'][0], 'property_action_category');
            if ($current_tipo) {
                array_push($filtros_buscados, $current_tipo->name);
                array_push($filter_taxs, array(
                    'taxonomy' => 'property_action_category',
                    'field' => 'slug',
                    'terms' => $_GET['filter_search_action'][0]
                ));
            }
        }
        if (!empty($_GET['filter_search_type']) && $_GET['filter_search_type'][0]!='') {
            $current_categoria = get_term_by('slug', $_GET['filter_search_type'][0], 'property_category');
            if ($current_categoria) {
                array_push($filtros_buscados, $current_categoria->name);
                array_push($filter_taxs, array(
                    'taxonomy' => 'property_category',
                    'field' => 'slug',
                    'terms' => $_GET['filter_search_type'][0]
                ));
            }
        }
        if (!empty($_GET['filter_departamentos']) && $_GET['filter_departamentos'][0]!='') {
            $current_departamento = get_term_by('slug', $_GET['filter_departamentos'][0], 'property_county_state');
            if ($current_departamento) {
                array_push($filtros_buscados, $current_departamento->name);
                array_push($filter_taxs, array(
                    'taxonomy' => 'property_county_state',
                    'field' => 'slug',
                    'terms' => $_GET['filter_departamentos'][0]
                ));
            }
        }
        if (!empty($_GET['filter_provincias']) && $_GET['filter_provincias'][0]!='') {
            $current_provincia = get_term_by('slug', $_GET['filter_provincias'][0], 'property_city');
            if ($current_provincia) {
                array_push($filtros_buscados, $current_provincia->name);
                array_push($filter_taxs, array(
                    'taxonomy' => 'property_city',
                    'field' => 'slug',
                    'terms' => $_GET['filter_provincias'][0]
                ));
            }
        }
        if (!empty($_GET['filter_distritos']) && $_GET['filter_distritos'][0]!='') {
            $current_distrito = get_term_by('slug', $_GET['filter_distritos'][0], 'property_area');
            if ($current_distrito) {
                array_push($filtros_buscados, $current_distrito->name);
                array_push($filter_taxs, array(
                    'taxonomy' => 'property_area',
                    'field' => 'slug',
                    'terms' => $_GET['filter_distritos'][0]
                ));
            }
        }
        if (!empty($_GET['area_desde'])) {
            array_push($filtros_buscados, $_GET['area_desde'].'m2');
            array_push($filter_metas, array(
                'key' => 'property_size',
                'compare' => '>=',
                'terms' => $_GET['area_desde']
            ));
        }
        if (!empty($_GET['area_hasta'])) {
            array_push($filtros_buscados, $_GET['area_hasta'].'m2');
            array_push($filter_metas, array(
                'key' => 'property_size',
                'compare' => '<=',
                'terms' => $_GET['area_hasta']
            ));
        }
        if (!empty($_GET['precio_desde'])) {
            array_push($filtros_buscados, '$'.$_GET['precio_desde']);
            array_push($filter_metas, array(
                'key' => 'property_price',
                'compare' => '>=',
                'terms' => $_GET['precio_desde']
            ));
        }
        if (!empty($_GET['precio_hasta'])) {
            array_push($filtros_buscados, '$'.$_GET['precio_hasta']);
            array_push($filter_metas, array(
                'key' => 'property_price',
                'compare' => '<=',
                'terms' => $_GET['precio_hasta']
            ));
        } ?>

        <?php $args = array(
            'posts_per_page' => -1,
            'post_type' => 'estate_property'
        );
        if (count($filter_taxs) > 0) $args['tax_query'] = $filter_taxs;
        if (count($filter_metas) > 0) $args['meta_query'] = $filter_metas;
        // echo '<pre>';print_r($args);
        // echo '</pre>';die;

        $propiedades = new WP_Query($args);

        if(pll_current_language() == 'en') {
            if ($propiedades->post_count == 1) { ?>
                <h2 class="entry-title"><?php printf( __( '%d match found for your search', 'madretierra' ), 1 ); ?></h2>
            <?php } else { ?>
                <h2 class="entry-title"><?php printf( __( '%d matches found for your search', 'madretierra' ), $propiedades->post_count ); ?></h2>
            <?php }
        } else {
            if ($propiedades->post_count == 1) { ?>
                <h2 class="entry-title"><?php printf( __( 'Se ha encontrado %d coincidencia para tu búsqueda', 'madretierra' ), 1 ); ?></h2>
            <?php } else { ?>
                <h2 class="entry-title"><?php printf( __( 'Se ha encontrado %d coincidencias para tu búsqueda', 'madretierra' ), $propiedades->post_count ); ?></h2>
            <?php }
        } ?>

        <h4 class="filtros"><?php echo implode(' - ', $filtros_buscados); ?></h4>

        <?php $tipos = get_terms(array(
            'taxonomy' => 'property_action_category',
            'hide_empty' => false
        ));
        $tipos_select_list = '';
        if ($tipos) {
            foreach ($tipos as $tipo) {
                $tipos_select_list .= '<li role="presentation" data-value="'.$tipo->slug.'">'.$tipo->name.'</li>';
            }
        }
        $categorias = get_terms(array(
            'taxonomy' => 'property_category',
            'hide_empty' => false
        ));
        $categ_select_list = '';
        if ($categorias) {
            foreach ($categorias as $categoria) {
                $categ_select_list .= '<li role="presentation" data-value="'.$categoria->slug.'">'.$categoria->name.'</li>';
            }
        }
        $departamentos = get_terms(array(
            'taxonomy' => 'property_county_state',
            'hide_empty' => false
        ));
        $departamentos_select_list = '';
        if ($departamentos) {
            foreach ($departamentos as $departamento) {
                $departamentos_select_list .= '<li role="presentation" data-value="'.$departamento->slug.'">'.$departamento->name.'</li>';
            }
        }
        $provincias = get_terms(array(
            'taxonomy' => 'property_city',
            'hide_empty' => false
        ));
        $provincias_select_list = '';
        if ($provincias) {
            foreach ($provincias as $provincia) {
                $provincias_select_list .= '<li role="presentation" data-value="'.$provincia->slug.'">'.$provincia->name.'</li>';
            }
        }
        $distritos = get_terms(array(
            'taxonomy' => 'property_area',
            'hide_empty' => false
        ));
        $distritos_select_list = '';
        if ($distritos) {
            foreach ($distritos as $distrito) {
                $distritos_select_list .= '<li role="presentation" data-value="'.$distrito->slug.'">'.$distrito->name.'</li>';
            }
        } ?>
        
        <!-- Listings starts here -->                   
        <div class="spinner" id="listing_loader">
            <div class="new_prelader"></div>
        </div>   
        <div id="listing_ajax_container" class="ajax12">
            <?php while ($propiedades->have_posts()) { $propiedades->the_post();
                $post_id = get_the_id();
                $imagenes = get_posts(array(
                    'post_type' => 'attachment',
                    'post_parent' => $post_id,
                    'showposts' => 20
                )); ?>
                <div class="col-md-12 has_prop_slider  listing_wrapper " 
                    data-org="4"   
                    data-main-modal="<?php the_post_thumbnail_url('large'); ?>"
                    data-modal-title="<?php the_title(); ?>"
                    data-modal-link="<?php the_permalink(); ?>"
                    data-listid="<?php the_ID(); ?>" >    
        
                    <div class="property_listing  property_card_default  " data-link="">    
                        <div class="listing-unit-img-wrapper">
                            <div class="prop_new_details">
                                <div class="prop_new_details_back"></div>       
                                <div class="property_media">
                                    <i class="fa fa-camera" aria-hidden="true"></i> <?php echo count($imagenes); ?>
                                </div>
                                <div class="property_location_image">
                                    <span class="property_marker"></span>
                                    <?php $ciudades = wp_get_post_terms($post_id, 'property_city');
                                    if ($ciudades) {
                                        foreach ($ciudades as $ciudad) { ?>
                                            <a href="<?php echo get_term_link($ciudad); ?>" rel="tag"><?php echo $ciudad->name; ?></a>
                                        <?php }
                                    } ?>
                                </div>
                                <div class="featured_gradient"></div>
                            </div> 
                            <div id="property_unit_carousel_5ef450b1ccc4c" class="carousel property_unit_carousel slide " data-ride="carousel" data-interval="false">
                                <div class="carousel-inner">
                                    <?php if ($imagenes) {
                                        $c = 0;
                                        foreach ($imagenes as $image) {
                                            $c++; ?>
                                            <div class="item <?php if($c==1){echo 'active';}else{echo 'lazy-load-item';} ?>">    
                                                <a href="<?php the_permalink(); ?>" target="_self"><?php echo wp_get_attachment_image( $image->ID, 'large' ); ?></a>     
                                            </div>
                                        <?php }
                                    } ?>
                                </div>
                                <a href="<?php the_permalink(); ?>" target="_self"> </a><a class="left  carousel-control" href="#property_unit_carousel_5ef450b1ccc4c" data-slide="prev">
                                    <i class="demo-icon icon-left-open-big"></i>
                                </a>
                                <a class="right  carousel-control" href="#property_unit_carousel_5ef450b1ccc4c" data-slide="next">
                                    <i class="demo-icon icon-right-open-big"></i>
                                </a>
                            </div>
                            <div class="tag-wrapper">
                                <?php if (get_post_meta($post_id, 'prop_featured', true)) echo '<div class="featured_div">'.__('Destacado', 'madretierra').'</div>'; ?>
                                <div class="status-wrapper">
                                    <?php $tipos = wp_get_post_terms($post_id, 'property_action_category');
                                    if ($tipos) {
                                        foreach ($tipos as $tipo) { ?>
                                            <div class="action_tag_wrapper <?php echo $tipo->name; ?> "><?php echo $tipo->name; ?></div>
                                        <?php }
                                    }
                                    $estados = wp_get_post_terms($post_id, 'property_status');
                                    if ($estados) {
                                        foreach ($estados as $estado) { ?>
                                            <div class="ribbon-inside <?php echo $estado->name; ?> "><?php echo $estado->name; ?></div>
                                        <?php }
                                    } ?>
                                </div>
                            </div>
                        </div>
                        <h4>    
                            <a href="<?php the_permalink(); ?>" target="_self ">
                                <?php the_title(); ?>
                            </a> 
                        </h4>
                        <div class="listing_unit_price_wrapper">
                            $ <?php echo get_post_meta($post_id, 'property_price', true); ?> <span class="price_label"></span>
                        </div>
                        <div class="listing_details the_list_view" style="display:block;"><?php the_excerpt(); ?></div>   
                        <div class="listing_details half_map_list_view" ><?php the_excerpt(); ?></div>   
                        <div class="listing_details the_grid_view" style="display:none;"><?php the_excerpt(); ?></div>
                        <div class="property_listing_details">
                            <span class="inforoom"><svg viewBox="0 0 90 50" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M16 18C16 13.6 19.6 10 24 10C28.4 10 32 13.6 32 18C32 22.4 28.4 26 24 26C19.6 26 16 22.4 16 18ZM88 30H12V2C12 0.9 11.1 0 10 0H2C0.9 0 0 0.9 0 2V50H12V42H78V50H80H82H86H88H90V32C90 30.9 89.1 30 88 30ZM74 12H38C36.9 12 36 12.9 36 14V26H88C88 18.3 81.7 12 74 12Z" fill="black"/>
                            </svg><?php echo get_post_meta($post_id, 'property_bedrooms', true); ?></span>
                            <span class="infobath"><svg  viewBox="0 0 56 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M2 37C2.00973 43.673 5.92011 49.724 12 52.4742V58C12.0016 58.5516 12.4484 58.9984 13 59H15C15.5516 58.9984 15.9984 58.5516 16 58V53.7186C16.9897 53.9011 17.9936 53.9953 19 54H37C38.0064 53.9953 39.0103 53.9011 40 53.7186V58C40.0016 58.5516 40.4484 58.9984 41 59H43C43.5516 58.9984 43.9984 58.5516 44 58V52.4742C50.0799 49.724 53.9903 43.673 54 37V31H2V37Z" fill="black"/>
                            <path d="M55 27H1C0.447715 27 0 27.4477 0 28C0 28.5523 0.447715 29 1 29H55C55.5523 29 56 28.5523 56 28C56 27.4477 55.5523 27 55 27Z" fill="black"/>
                            <path d="M5 21H7V22C7 22.5523 7.44772 23 8 23C8.55228 23 9 22.5523 9 22V18C9 17.4477 8.55228 17 8 17C7.44772 17 7 17.4477 7 18V19H5V7C5 4.23858 7.23858 2 10 2C12.7614 2 15 4.23858 15 7V7.09021C12.116 7.57866 10.004 10.0749 10 13C10.0016 13.5516 10.4484 13.9984 11 14H21C21.5516 13.9984 21.9984 13.5516 22 13C21.996 10.0749 19.884 7.57866 17 7.09021V7C17 3.13401 13.866 0 10 0C6.13401 0 3 3.13401 3 7V25.5H5V21Z" fill="black"/>
                            </svg><?php echo get_post_meta($post_id, 'property_bathrooms', true); ?></span>
                            <span class="infosize"><svg  viewBox="0 0 42 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M41 0H13C12.45 0 12 0.45 12 1V10H1C0.45 10 0 10.45 0 11V31C0 31.55 0.45 32 1 32H29C29.55 32 30 31.55 30 31V22H41C41.55 22 42 21.55 42 21V1C42 0.45 41.55 0 41 0ZM28 30H2V12H28V30ZM40 20H30V11C30 10.45 29.55 10 29 10H14V2H40V20Z" fill="black"/>
                            </svg><?php echo get_post_meta($post_id, 'property_size', true); ?> m<sup>2</sup></span>
                            <?php if(pll_current_language() == 'en') { ?>
                                <a href="<?php the_permalink(); ?>" target="_self"  class="unit_details_x">details</a>
                            <?php } else { ?>
                                <a href="<?php the_permalink(); ?>" target="_self"  class="unit_details_x">detalles</a>
                            <?php } ?>
                        </div>               
                        <div class="property_location">
                            <div class="property_agent_wrapper">
                                <div class="property_agent_image" style="background-image:url('https://www.madretierra.pe/wp-content/themes/wpresidence/img/default-user_1.png')"></div> 
                                <div class="property_agent_image_sign"><i class="fa fa-user-circle-o" aria-hidden="true"></i></div>
                            </div>
                            <div class="listing_actions">
                            <div class="share_unit">
                                <?php $titulo_mas = str_replace(' ', '+', get_the_title()); ?>
                                <a href="https://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&amp;t=<?php echo $titulo_mas; ?>" target="_blank" class="social_facebook"></a>
                                <a href="https://twitter.com/intent/tweet?text=<?php echo $titulo_mas; ?>+<?php the_permalink(); ?>%2F" class="social_tweet" target="_blank"></a>
                                <a href="https://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php the_post_thumbnail_url('large'); ?>&amp;description=<?php echo $titulo_mas; ?>" target="_blank" class="social_pinterest"></a>
                                <a href="https://api.whatsapp.com/send?text=<?php echo $titulo_mas; ?>+<?php the_permalink(); ?>%2F" class="social_whatsup" target="_blank"></a>   
                                <a href="mailto:email@email.com?subject=<?php echo $titulo_mas; ?>&amp;body=<?php the_permalink(); ?>%2F" data-action="share email"  class="social_email"></a>
                            </div>
                            <span class="share_list"  data-original-title="compartir" ></span>
                            <span class="icon-fav icon-fav-off" data-original-title="agregar a favoritos" data-postid="<?php the_ID(); ?>"></span>
                            <span class="compare-action" data-original-title="comparar" data-pimage="https://www.madretierra.pe/wp-content/uploads/2020/06/dd1-143x83.jpg" data-pid="<?php the_ID(); ?>"></span>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <!-- Listings Ends  here --> 
        
        
        
        <div class="single-content"></div>


    </div>
</div>
    
<?php   include get_theme_file_path('sidebar.php'); ?>
       
    
</div>   

<?php 

 
$mapargs = array(
        'post_type'         =>  'estate_property',
        'post_status'       =>  'publish',
        'p'                 =>  $post->ID ,
        'fields'            =>    'ids');
  
$selected_pins  =   wpestate_listing_pins('blank_single',0,$mapargs,1);

wp_localize_script('googlecode_property', 'googlecode_property_vars2', 
            array('markers2'          =>  $selected_pins));


get_footer(); ?>