<?php
// Sigle - Blog post
// Wp Estate Pack
global $post;
get_header(); 
$wpestate_options=wpestate_page_details($post->ID); 
?>
<div class="row">
    <?php get_template_part('templates/breadcrumbs'); ?>
    <div class="col-xs-12 <?php print esc_html($wpestate_options['content_class']);?> single_width_page">
        
         <?php get_template_part('templates/ajax_container'); ?>
        
        <?php while (have_posts()) : the_post(); ?>
            <?php if (esc_html( get_post_meta($post->ID, 'page_show_title', true) ) != 'no') { ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
            <?php } ?>         
            <div class="single-content"><?php the_content();?></div><!-- single content-->



                <?php if (is_page(28897)) { ?>
                    <div class="woocommerce columns-4">
                        
                        <?php $productos = new WP_Query(array(
                            'post_type' => 'product'
                        ));
                        if ( $productos->have_posts() ) {

                            do_action( 'woocommerce_before_shop_loop' );
                            woocommerce_product_loop_start();

                            while ( $productos->have_posts() ) { $productos->the_post();

                                /**
                                 * Hook: woocommerce_shop_loop.
                                 */
                                do_action( 'woocommerce_shop_loop' );

                                wc_get_template_part( 'content', 'product' );
                            }

                            woocommerce_product_loop_end();
                            do_action( 'woocommerce_after_shop_loop' );

                        } else {
                            do_action( 'woocommerce_no_products_found' );
                        } ?>
                    </div>
                 <?php } ?>



        <!-- #comments start-->
        <?php 
        if ( comments_open() || get_comments_number() ) :
            comments_template('', true);
        endif;
        ?>	
        <!-- end comments -->   
        
        <?php endwhile; // end of the loop. ?>
    </div>
  
<?php   include get_theme_file_path('sidebar.php'); ?>
</div>   
<?php get_footer(); ?>