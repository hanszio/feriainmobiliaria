<!-- begin sidebar -->
<div class="clearfix visible-xs"></div>
<?php 
if( 
  is_page_template('buscador.php') ||
  ( !empty($wpestate_options) && ('no sidebar' != $wpestate_options['sidebar_class']) && ('' != $wpestate_options['sidebar_class'] ) && ('none' != $wpestate_options['sidebar_class']) )
){
?>    
    <div class="col-xs-12 <?php if(!empty($wpestate_options)) print esc_html($wpestate_options['sidebar_class']);?> widget-area-sidebar" id="primary" >
        <div id="primary_sidebar_wrapper">
            <?php 
            if('estate_property' == get_post_type() && !is_tax() && !is_page_template('buscador.php') ){
                $sidebar_agent_option_value=    get_post_meta($post->ID, 'sidebar_agent_option', true);    
                if($sidebar_agent_option_value =='global'){
                    $enable_global_property_page_agent_sidebar= esc_html ( wpresidence_get_option('wp_estate_global_property_page_agent_sidebar','') );
                    if($enable_global_property_page_agent_sidebar=='yes'){
                        include( locate_template ('/templates/property_list_agent.php') ); 
                    }
                }elseif ($sidebar_agent_option_value =='yes') {
                     include( locate_template ('/templates/property_list_agent.php') );
                }
                $post_author_id = get_post_field( 'post_author', get_the_id() );
                if ($post_author_id == 1) { ?>
                    <button class="wpresidence_button" id="reservar_propiedad" data-toggle="modal" data-target="#reservaModal" style="width:100%;">Reservar propiedad</button>
                <?php }

            } else if (is_page_template('buscador.php')) {
              dynamic_sidebar('primary-widget-area');
            }
            ?>

            <?php
            if (!empty($wpestate_options) && is_active_sidebar( $wpestate_options['sidebar_name'] ) ) { ?>
                <ul class="xoxo">
                    <?php dynamic_sidebar( $wpestate_options['sidebar_name'] ); ?>
                </ul>
            <?php 
            }

            get_template_part('templates/similar_listings');
            ?>
        </div>
    </div>   
<?php
}
?>
<!-- end sidebar -->