</div><!-- end content_wrapper started in header -->



</div> <!-- end class container -->
<?php
$show_sticky_footer_select  =   wpresidence_get_option('wp_estate_show_sticky_footer','');
$footer_background          =   wpresidence_get_option('wp_estate_footer_background','url');
$repeat_footer_back_status  =   wpresidence_get_option('wp_estate_repeat_footer_back','');
$logo_header_type           =   wpresidence_get_option('wp_estate_logo_header_type','');
$footer_style               =   '';
$footer_back_class          =   '';



if ($footer_background!=''){
    $footer_style='style=" background-image: url('.esc_url($footer_background).') "';
}

if( $repeat_footer_back_status=='repeat' ){
    $footer_back_class = ' footer_back_repeat ';
}else if( $repeat_footer_back_status=='repeat x' ){
    $footer_back_class = ' footer_back_repeat_x ';
}else if( $repeat_footer_back_status=='repeat y' ){
    $footer_back_class = ' footer_back_repeat_y ';
}else if( $repeat_footer_back_status=='no repeat' ){
    $footer_back_class = ' footer_back_repeat_no ';
}

if($show_sticky_footer_select=='yes'){
    $footer_back_class.=' sticky_footer ';
}

if($logo_header_type=='type4'){
    $footer_back_class.= ' footer_header4 ';
}

$show_foot          =   wpresidence_get_option('wp_estate_show_footer','');
$wide_footer        =   wpresidence_get_option('wp_estate_wide_footer','');
$wide_footer_class  =   '';

if($show_foot==''){
    $show_foot='yes';
}

$post_id='';
if( isset($post->ID) ){
   $post_id =$post->ID;
}



if( $show_foot=='yes' && !wpestate_half_map_conditions ($post_id) ){
   
    
    $wide_status     =   esc_html(wpresidence_get_option('wp_estate_wide_status',''));
    if($wide_status==''){
        $wide_status=1;
    }
    if($wide_status==2 || $wide_status==''){
        $footer_back_class.=" boxed_footer ";
    }

?>  
    <footer id="colophon" <?php print wp_kses_post($footer_style); ?> class=" <?php print esc_attr($footer_back_class);?> ">    

        <?php 
        if($wide_footer=='yes'){
            $wide_footer_class=" wide_footer ";
        }
        ?>
        
        <div id="footer-widget-area" class="row <?php print esc_attr($wide_footer_class);?>">
           <?php get_sidebar('footer');?>
        </div>

        
        <?php     
        $show_show_footer_copy_select  =   wpresidence_get_option('wp_estate_show_footer_copy','');
        if($show_show_footer_copy_select=='yes'){
        ?>
            <div class="sub_footer">  
                <div class="sub_footer_content <?php print esc_attr($wide_footer_class);?>">
                    <span class="copyright">
                        <?php   
                        $message = stripslashes( esc_html (wpresidence_get_option('wp_estate_copyright_message', '')) );
                        if (function_exists('icl_translate') ){
                            $property_copy_text      =   icl_translate('wpestate','wp_estate_copyright_message', $message );
                            print esc_html($property_copy_text);
                        }else{
                            print esc_html($message);
                        }
                        ?>
                    </span>

                    <div class="subfooter_menu">
                        <?php      
                            show_support_link();
                            wp_nav_menu( array(
                                'theme_location'    => 'footer_menu',
                            ));  
                        ?>
                    </div>  
                </div>  
            </div>      
        <?php
        }// end show subfooter
        ?>
        
     
    </footer><!-- #colophon -->
<?php } 
    
$ajax_nonce_log_reg = wp_create_nonce( "wpestate_ajax_log_reg" );
print'<input type="hidden" id="wpestate_ajax_log_reg" value="'.esc_html($ajax_nonce_log_reg).'" />    ';  

 ?><!--  get_template_part('templates/footer_buttons');-->
<?php get_template_part('templates/navigational');?>

<?php wp_get_schedules(); ?>




<?php 
global $wpestate_logo_header_align;
$logo_header_type            =   wpresidence_get_option('wp_estate_logo_header_type','');
$wpestate_logo_header_align  =   wpresidence_get_option('wp_estate_logo_header_align','');

if($logo_header_type=='type3'){ 
     include( locate_template( 'templates/top_bar_sidebar.php') ); 
}
?>
</div> <!-- end website wrapper -->

<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="contactModalLabel">Bienvenido(a) a Madre Tierra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Ingresa tus datos y nos pondremos en contacto muy pronto para darte a conocer noticias y novedades que tiene para ti MADRE TIERRA<br>&nbsp;</p>
        <div class="form-group">
          <label for="nombre">Nombre</label>
          <input type="text" class="form-control" name="nombre" id="nombre" />
        </div>
        <div class="form-group">
          <label for="email">Email</label>
          <input type="email" class="form-control" name="email" id="email" />
        </div>
        <div class="form-group">
          <label for="telefono">Teléfono</label>
          <input type="text" class="form-control" name="telefono" id="telefono" />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="guardar-contacto">Enviar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="donacionModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <form method="get" class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="contactModalLabel">Contribuir con Madre Tierra</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Ingrese su monto a donar<br>&nbsp;</p>
        <div class="form-group">
          <label for="nombre">Donación</label>
          <input type="number" min="1" class="form-control" name="donacion" id="donacion" />
        </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" name="add-to-cart" value="29209">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary" id="guardar-contacto">Donar</button>
      </div>
    </form>
  </div>
</div>

<?php if (is_singular('estate_property')) { ?>
  <div class="modal fade" id="reservaModal" tabindex="-1" role="dialog" aria-labelledby="contactModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="contactModalLabel">Reservar propiedad</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Ingresa tus datos para reservar la propiedad y nos pondremos en contacto con usted:<br>&nbsp;</p>
          <div class="form-group">
            <label for="propiedad">Propiedad</label>
            <input type="text" class="form-control" name="propiedad" id="propiedad" readonly value="<?php the_title(); ?>" />
          </div>
          <div class="form-group">
            <label for="nombre">Nombre completo</label>
            <input type="text" class="form-control" name="nombre" id="nombre3" />
          </div>
          <div class="form-group">
            <label for="email">Correo eléctronico</label>
            <input type="email" class="form-control" name="email" id="email3" />
          </div>
          <div class="form-group">
            <label for="telefono">Número teléfonico</label>
            <input type="text" class="form-control" name="telefono" id="telefono3" />
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
          <button type="button" class="btn btn-primary" id="enviar-reserva">Enviar reserva</button>
        </div>
      </div>
    </div>
  </div>
<?php } ?>

<?php  
get_template_part('templates/compare_list');
get_template_part('templates/login_register_modal');
if(is_singular('estate_property')){
    include( locate_template ('/templates/image_gallery.php') ); 
}

$ajax_nonce = wp_create_nonce( "wpestate_ajax_filtering" );
print'<input type="hidden" id="wpestate_ajax_filtering" value="'.esc_html($ajax_nonce).'" />    ';

$ajax_nonce_pay = wp_create_nonce( "wpestate_payments_nonce" );
print'<input type="hidden" id="wpestate_payments_nonce" value="'.esc_html($ajax_nonce_pay).'" />    ';

if(wpestate_is_property_modal()){
    get_template_part('templates/property_details_modal');
}

wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<?php if (is_front_page()) { ?>
  <script>
  jQuery(function($) {
    if ($.cookie('modal_contactModal') == null) {
        $.cookie('modal_contactModal', 'yes', { expires: 7, path: '/' });
        $('#contactModal').modal('show');
    }
    $('#ciudades_select_list li').click(function() {
      $('#provincias_select_list li').hide()
      let name = $(this).data('name')
      console.log('#provincias_select_list li[data-stateparent="'+name+'"]', name)
      $('#provincias_select_list li[data-stateparent="'+name+'"]').show()
    })
    $('#provincias_select_list li').click(function() {
      $('#distritos_select_list li').hide()
      let name = $(this).data('name')
      console.log('#distritos_select_list li[data-cityparent="'+name+'"]', name)
      $('#distritos_select_list li[data-cityparent="'+name+'"]').show()
    })
    $('#donacion').click(function(e) {
      e.preventDefault()
      $('#donacionModal').modal('show');
    })
    $('#guardar-contacto').click(function() {
      let nombre = $('#nombre').val()
      let email = $('#email').val()
      let telefono = $('#telefono').val()
      if (nombre=='' || email=='' || telefono=='') {
        alert('Todos los campos son obligatorios')
        return
      }
      $.post('<?php echo admin_url('admin-ajax.php'); ?>', {
          action : 'guardar_contacto',
          nombre : nombre,
          telefono : telefono,
          email : email,
          mensaje : '',
          wpnonce : '<?php echo wp_create_nonce('guardar_contacto'); ?>'
      }, function(response){
          alert('Se guardaron sus datos correctamente')
          $('#contactModal').modal('hide')
      });
    })
  })
  </script>
<?php } else if (is_singular('estate_property')) { ?>
  <script>
  jQuery(function($) {
    $('#enviar-reserva').click(function() {
      let propiedad = $('#propiedad').val()
      let nombre = $('#nombre3').val()
      let email = $('#email3').val()
      let telefono = $('#telefono3').val()
      if (nombre=='' || email=='' || telefono=='') {
        alert('Todos los campos son obligatorios')
        return
      }
      $.post('<?php echo admin_url('admin-ajax.php'); ?>', {
          action : 'enviar_reserva',
          propiedad : propiedad,
          nombre : nombre,
          telefono : telefono,
          email : email,
          wpnonce : '<?php echo wp_create_nonce('enviar_reserva'); ?>'
      }, function(response){
          alert('Se envió su reserva. Nos pondremos en contacto con usted pronto')
          $('#reservaModal').modal('hide')
      });
    })
  })
  </script>
<?php } ?>
<script>
  jQuery(function($) {
    if ($('#commentform').length > 0) {
      $('#reply-title').text('<?php _e('Déjanos tu comentario', 'madretierra'); ?>')
      $('.comment-form-comment').insertAfter($('.comment-form-url'))
    }
    if ($('#tipo-estacionamientos').length > 0) {
      $('#tipo-estacionamientos option[value="Not Available"]').text('Ninguno')
    }
    $('#footer-widget-area .social_sidebar_internal').append('<a href="https://wa.me/51949238050?text=Hola,%20estoy%20interesado%20en%20adquirir%20una%20propiedad" target="_blank"><i class="fa fa-whatsapp  fa-fw"></i></a>')
    $('#guardar-contacto2').click(function() {
      let nombre = $('#nombre2').val()
      let email = $('#email2').val()
      let telefono = $('#telefono2').val()
      let mensaje = $('#mensaje2').val()
      if (nombre=='' || email=='' || telefono=='') {
        alert('Todos los campos son obligatorios')
        return
      }
      $.post('<?php echo admin_url('admin-ajax.php'); ?>', {
          action : 'guardar_contacto',
          nombre : nombre,
          telefono : telefono,
          email : email,
          mensaje : mensaje,
          wpnonce : '<?php echo wp_create_nonce('guardar_contacto'); ?>'
      }, function(response){
          alert('Se guardaron sus datos correctamente')
      });
    })
    if ($('#property_county').length > 0) {
      $('#property_county').change(function() {
        let dpto = $(this).val()
        $.post('<?php echo admin_url('admin-ajax.php'); ?>', {
            action : 'get_dpto',
            dpto : dpto,
            wpnonce : '<?php echo wp_create_nonce('get_dpto'); ?>'
        }, function(response){
            $('#property_city_submit').html(response)
        });
      })
      $('#property_city_submit').change(function() {
        let dpto = $('#property_county').val()
        let prov = $(this).val()
        $.post('<?php echo admin_url('admin-ajax.php'); ?>', {
            action : 'get_prov',
            dpto : dpto,
            prov : prov,
            wpnonce : '<?php echo wp_create_nonce('get_prov'); ?>'
        }, function(response){
            $('#property_area').html(response)
        });
      })
    }
    $('.page-template-user_dashboard_profile h3.entry-title').text('<?php _e('Panel de control: Página de perfil', 'madretierra'); ?>')
    $('.page-template-user_dashboard_add h3.entry-title').text('<?php _e('Panel de control: Agregar propiedad', 'madretierra'); ?>')
    $('.page-template-user_dashboard_inbox h3.entry-title').text('<?php _e('Panel de control: Bandeja de entrada', 'madretierra'); ?>')
  })
  </script>
</body>
</html>