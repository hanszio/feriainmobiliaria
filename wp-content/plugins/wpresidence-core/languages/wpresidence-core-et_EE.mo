��    ;      �  O   �           	          '     A     N     ^  	   j  
   t  	        �     �  	   �     �     �  
   �  
   �     �  	   �     �     �     �  	          
   (     3     ;  
   O     Z     _          �     �     �     �     �     �     �     �                "     +     :  
   B     M     T     a     n     {     �     �     �  
   �     �  	   �  
   �     �     �  �  �     �
     �
     �
          &     3     H     X     e     t     z  
   �  
   �     �     �     �     �     �  	   �     �     �     �     �          
          (     5     :     T     [     m     {     �     �     �     �     �     �     �     �     �     �     �                          3     E     N     [     h     n     z          �     �            .   /          !      0           9   ,            #            "   2   -         8           $                 :   4      1   
         +              *       '                 %                     6             3   7                 ;   &          (   )         5   	        Add New City Add New Property Add New Property Category Add Property Advanced Search All Actions All Areas All Cities All Types Amenities and Features Area Calculate Category City Contact Me Contact Us Country Country:  Default Edit Enter Your Email Address Favorites Interest Rate in % Item Price Log Out Mortgage Calculator My Profile Name Need an account? Register here! Password Password Reset Request Percent Down Phone Please add your Twitter ID! Price Price High to Low Price Low to High Price:  Property Address Property Details Register Reset Password Rooms:  Sale Price Search Send Message Term (Years) Type Keyword Type your message... Upload Image Username Your Address Your Email Your Message Your Name Your Phone add to favorites favorite Project-Id-Version: Wp Residence 1.04 v1.04
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/wpresidence-core
POT-Creation-Date: 2019-05-10 13:33+0300
PO-Revision-Date: 2019-05-10 13:33+0300
Last-Translator: Tarmo <info@omakodu.eu>
Language-Team: 
Language: et_EE
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n != 1;
X-Generator: Poedit 1.8.8
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: .
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: E:/!our theme wordpress/residence/!versions/1.04x/wpresidence
 Lisa uus linn Lisa uus kuulutus Lisa uus kuulutuste kategooria Lisa kuulutus Kiire otsing Kõik tehingu liigid Linnaosa/asulad Kõik linnad Kõik tüübid Lisad Linnaosa/asula Kalkuleeri Kategooria Linn Kontakteeru minuga Kontakteeru Riik Riik: Vaikimisi Muuda Sisesta Email Lemmikud Intressimäär Hind: Logi välja Laenu kalkulaator Minu profiil Nimi Pole kontot? Registreeru! Parool Unustasin parooli Laenuprotsent Telefon Lisa enda Twitteri ID! Hind Hind kahanevalt Hind kasvavalt Hind: Aadress Ükskikasjad Registreeru Muuda parool Ruumid: Summa Otsi Saada sõnum Mitu aastat Sisesta otsingusõna Sisesta sõnum... Lae pilt Kasutajanimi Sinu aadress Email Sinu sõnum Nimi Telefon Lisa lemmikuks meeldib 